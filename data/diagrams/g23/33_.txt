
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Description" as thingdescription
	entity "Ead" as thingEAD
	actor "Archivist" as actorarchivist
	boundary "Ead\nInterface" as thingeadinterface #grey
	control "Export A\nDescription As\nEad" as controlexportdescription

	thingEAD <.. thingdescription
	actorarchivist --- thingeadinterface
	thingdescription --- controlexportdescription
	thingEAD --- controlexportdescription
	thingeadinterface --> controlexportdescription

@enduml