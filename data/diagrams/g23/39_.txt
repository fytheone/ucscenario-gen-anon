
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ead" as thingEAD
	entity "Import" as thingimport
	entity "Frontend Application" as thingfrontendapplication #grey
	actor "Archivist" as actorarchivist
	boundary "Application\nInterface" as thingapplicationinterface #grey
	control "Upload An\nEad For\nImport Within\nThe Frontend\nApplication" as controluploadEADimport

	thingimport <.. thingEAD
	thingfrontendapplication <.. thingimport
	actorarchivist --- thingapplicationinterface
	thingEAD --- controluploadEADimport
	thingimport --- controluploadEADimport
	thingfrontendapplication --- controluploadEADimport
	thingapplicationinterface --> controluploadEADimport

@enduml