
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Change" as thingchange
	entity "Database" as thingdatabase
	entity "Enum Value\nLists" as thingenumvaluelists
	actor "User" as actoruser
	boundary "Change\nInterface" as thingchangeinterface #grey
	boundary "Database\nInterface" as thingdatabaseinterface #grey
	control "Support" as controlsupportloadingenumvaluelistsdatabase
	control "Loading Enum\nValue Lists\nFrom The\nDatabase" as controlloadingenumvaluelistsdatabase
	control "Backend Changes" as controlbackendchangesupportloadingenumvaluelistsdatabase

	thingdatabase <.. thingenumvaluelists
	actoruser --- thingchangeinterface
	actoruser --- thingdatabaseinterface
	controlbackendchangesupportloadingenumvaluelistsdatabase --> controlsupportloadingenumvaluelistsdatabase
	thingchangeinterface --> controlsupportloadingenumvaluelistsdatabase
	controlsupportloadingenumvaluelistsdatabase --> controlloadingenumvaluelistsdatabase
	thingenumvaluelists --- controlloadingenumvaluelistsdatabase
	thingdatabase --- controlloadingenumvaluelistsdatabase
	thingdatabaseinterface --> controlloadingenumvaluelistsdatabase
	thingchange --- controlbackendchangesupportloadingenumvaluelistsdatabase
	thingchangeinterface --> controlbackendchangesupportloadingenumvaluelistsdatabase

@enduml