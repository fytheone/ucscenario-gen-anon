
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset Instances" as thingdatasetinstances #grey
	entity "Type" as thingtype
	entity "Dataset" as thingdataset #grey
	actor "Dataset Developer" as actordatasetdeveloper
	circle "New Version" as thingnewversion
	boundary "Instances\nInterface" as thinginstancesinterface #grey
	control "Deploy A\nNew Version\nOf A\nDataset Type\nAffecting The\nDataset Instances\nOf That\nType" as controldeploynewversiondatasetinstances

	thingtype <.. thingdatasetinstances
	thingdataset <.. thingtype
	thingdatasetinstances <.. thingtype
	thingtype *-- thingnewversion
	actordatasetdeveloper --- thinginstancesinterface
	thingnewversion --- controldeploynewversiondatasetinstances
	thingdatasetinstances --- controldeploynewversiondatasetinstances
	thinginstancesinterface --> controldeploynewversiondatasetinstances
	thingtype --- controldeploynewversiondatasetinstances

@enduml