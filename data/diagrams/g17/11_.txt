
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Application" as thingapplication
	entity "Option" as thingoption
	entity "Runtime" as thingruntime
	entity "Dataset Code" as thingdatasetcode #grey
	actor "Dataset Developer" as actordatasetdeveloper
	boundary "Runtime\nInterface" as thingruntimeinterface #grey
	control "Have" as controlhave
	control "Have The\nOption Forcing\nApplications The\nDataset Code\nInjected At\nRuntime" as controlhaveoption

	thingdatasetcode <.. thingapplication
	thingapplication <.. thingoption
	thingdatasetcode <.. thingoption
	thingruntime <.. thingoption
	thingruntime <.. thingdatasetcode
	actordatasetdeveloper --- thingruntimeinterface
	controlhaveoption --> controlhave
	thingruntimeinterface --> controlhave
	thingoption --- controlhaveoption
	thingapplication --- controlhaveoption
	thingdatasetcode --- controlhaveoption
	thingruntime --- controlhaveoption
	thingruntimeinterface --> controlhaveoption

@enduml