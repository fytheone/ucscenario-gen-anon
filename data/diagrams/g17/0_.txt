
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "App" as thingapp
	entity "Dataset" as thingdataset
	entity "Type" as thingtype
	actor "App Developer" as actorappdeveloper
	circle "Code" as thingcode
	boundary "App\nInterface" as thingappinterface #grey
	control "Include The\nCode Of\nA Dataset\nType In\nApp" as controlincludecode
	control "Create A\nDataset Of\nThat Type\nWhen Deploying\nThe App" as controlcreatedatasetapp

	thingtype *-- thingdataset
	thingapp *-- thingdataset
	thingapp <.. thingtype
	thingdataset <.. thingtype
	thingdataset *-- thingcode
	thingtype *-- thingcode
	actorappdeveloper --- thingappinterface
	thingcode --- controlincludecode
	thingapp --- controlincludecode
	thingappinterface --> controlincludecode
	thingtype --- controlincludecode
	thingdataset --- controlcreatedatasetapp
	thingtype --- controlcreatedatasetapp
	thingapp --- controlcreatedatasetapp
	thingappinterface --> controlcreatedatasetapp

@enduml