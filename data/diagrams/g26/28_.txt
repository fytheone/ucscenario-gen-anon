
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Level" as thinglevel
	entity "Scan File" as thingscannedfile
	entity "Automatic ,\nContrast Operation" as thingautomaticcontrastoperation
	actor "Archivist" as actorarchivist
	boundary "File\nInterface" as thingfileinterface #grey
	boundary "Level\nInterface" as thinglevelinterface #grey
	control "Apply Automatic,\nContrast Operations\nTo A\nScanned File" as controlapplyautomaticcontrastoperationscannedfile
	control "Apply Level" as controlapplylevel

	thingscannedfile <.. thingautomaticcontrastoperation
	actorarchivist --- thingfileinterface
	actorarchivist --- thinglevelinterface
	thingautomaticcontrastoperation --- controlapplyautomaticcontrastoperationscannedfile
	thingscannedfile --- controlapplyautomaticcontrastoperationscannedfile
	thingfileinterface --> controlapplyautomaticcontrastoperationscannedfile
	thinglevel --- controlapplylevel
	thinglevelinterface --> controlapplylevel

@enduml