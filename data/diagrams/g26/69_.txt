
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Outreach Materials" as thingoutreachmaterials
	actor "Researcher" as actorresearcher
	boundary "Materials\nInterface" as thingmaterialsinterface #grey
	control "Access Educational\nOutreach Materials" as controlaccesseducationaloutreachmaterials

	actorresearcher --- thingmaterialsinterface
	thingoutreachmaterials --- controlaccesseducationaloutreachmaterials
	thingmaterialsinterface --> controlaccesseducationaloutreachmaterials

@enduml