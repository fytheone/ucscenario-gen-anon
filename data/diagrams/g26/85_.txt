
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Right" as thingright
	entity "Image" as thingimage
	actor "Archivist" as actorarchivist
	boundary "Right\nInterface" as thingrightinterface #grey
	control "Search Images\nBy Rights" as controlsearchimageright

	thingright <.. thingimage
	actorarchivist --- thingrightinterface
	thingimage --- controlsearchimageright
	thingright --- controlsearchimageright
	thingrightinterface --> controlsearchimageright

@enduml