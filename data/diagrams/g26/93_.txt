
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Groups" as thingusergroups #grey
	actor "Administrator" as actoradministrator
	boundary "Groups\nInterface" as thinggroupsinterface #grey
	control "Create User\nGroups" as controlcreateusergroups

	actoradministrator --- thinggroupsinterface
	thingusergroups --- controlcreateusergroups
	thinggroupsinterface --> controlcreateusergroups

@enduml