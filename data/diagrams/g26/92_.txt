
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Image" as thingimage
	actor "Archivist" as actorarchivist
	boundary "Image\nInterface" as thingimageinterface #grey
	control "Search Images\nUploading An\nImage" as controlsearchimageimage

	actorarchivist --- thingimageinterface
	thingimageinterface --> controlsearchimageimage
	thingimage --- controlsearchimageimage

@enduml