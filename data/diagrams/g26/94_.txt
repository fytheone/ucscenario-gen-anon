
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Roles" as thinguserroles #grey
	actor "Administrator" as actoradministrator
	boundary "Roles\nInterface" as thingrolesinterface #grey
	control "Create User\nRoles" as controlcreateuserroles

	actoradministrator --- thingrolesinterface
	thinguserroles --- controlcreateuserroles
	thingrolesinterface --> controlcreateuserroles

@enduml