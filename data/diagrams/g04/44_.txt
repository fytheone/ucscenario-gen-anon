
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dashboard" as thingdashboard
	entity "Usage Stats" as thingusagestats #grey
	entity "Location" as thinglocation
	actor "Admin" as actoradmin
	boundary "Location\nInterface" as thinglocationinterface #grey
	boundary "Stats\nInterface" as thingstatsinterface #grey
	control "Have A\nDashboard Shows\nLocation" as controlhavedashboardlocation
	control "Have A\nDashboard Shows\nUsage Stats" as controlhavedashboard

	thingusagestats <.. thingdashboard
	thinglocation <.. thingdashboard
	actoradmin --- thinglocationinterface
	actoradmin --- thingstatsinterface
	thinglocation --- controlhavedashboardlocation
	thinglocationinterface --> controlhavedashboardlocation
	thingdashboard --- controlhavedashboardlocation
	thingdashboard --- controlhavedashboard
	thingusagestats --- controlhavedashboard
	thingstatsinterface --> controlhavedashboard

@enduml