
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Zip Code" as thingzipcode #grey
	entity "Recycling Facilities" as thingrecyclingfacilities
	actor "User" as actoruser
	boundary "Code\nInterface" as thingcodeinterface #grey
	boundary "Facilities\nInterface" as thingfacilitiesinterface #grey
	control "Enter Zip\nCode" as controlenterzipcode
	control "Get A\nList Of\nNearby Recycling\nFacilities" as controlgetlist

	actoruser --- thingcodeinterface
	actoruser --- thingfacilitiesinterface
	thingzipcode --- controlenterzipcode
	thingcodeinterface --> controlenterzipcode
	thingrecyclingfacilities --- controlgetlist
	thingfacilitiesinterface --> controlgetlist

@enduml