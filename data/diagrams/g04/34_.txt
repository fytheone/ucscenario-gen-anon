
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Recycling Centers" as thingrecyclingcenters #grey
	entity "Map" as thingmap
	actor "User" as actoruser
	circle "Location" as thinglocation
	boundary "Map\nInterface" as thingmapinterface #grey
	control "View All\nLocations Of\nRecycling Centers\nOn A\nMap" as controlviewlocationmap

	thingmap <.. thingrecyclingcenters
	thingrecyclingcenters *-- thinglocation
	actoruser --- thingmapinterface
	thinglocation --- controlviewlocationmap
	thingrecyclingcenters --- controlviewlocationmap
	thingmap --- controlviewlocationmap
	thingmapinterface --> controlviewlocationmap

@enduml