
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Grant Proposals" as thinggrantproposals #grey
	entity "Plan" as thingplan #grey
	entity "Data Management" as thingdatamanagement #grey
	entity "Disaster Recovery" as thingdisasterrecovery #grey
	actor "Datum Contributor" as actordatacontributor
	boundary "Plan\nInterface" as thingplaninterface #grey
	boundary "Proposals\nInterface" as thingproposalsinterface #grey
	control "Refer To\nThe Plan\nIn Data\nManagement Plan" as controlreferplandatamanagementplan
	control "Refer To\nThe Disaster\nRecovery Plan\nIn Grant\nProposals" as controlreferdisasterrecoveryplan

	thingdatamanagement <.. thingplan
	thingdisasterrecovery <.. thingplan
	thinggrantproposals <.. thingplan
	actordatacontributor --- thingplaninterface
	actordatacontributor --- thingproposalsinterface
	thingplan --- controlreferplandatamanagementplan
	thingplaninterface --> controlreferplandatamanagementplan
	thinggrantproposals --- controlreferdisasterrecoveryplan
	thingproposalsinterface --> controlreferdisasterrecoveryplan
	thingplan --- controlreferdisasterrecoveryplan

@enduml