
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Item" as thingitem
	entity "Repository" as thingrepository
	actor "Repository Manager" as actorrepositorymanager
	boundary "Way" as thingwaytrackitemrepositoryrepositorymanager
	control "Track" as controltrack

	actorrepositorymanager --- thingwaytrackitemrepositoryrepositorymanager
	thingwaytrackitemrepositoryrepositorymanager --> controltrack

@enduml