
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Access" as thingaccess
	entity "User" as thinguser
	entity "Restrict Object" as thingrestrictedobject
	actor "Collection Curator" as actorcollectioncurator
	boundary "Repository" as thingrepositoryrequestaccesscollectioncurator #grey
	control "Contact From" as controlcontactrepositoryrequestaccesscollectioncurator
	control "Request Access\nTo Restricted\nObjects" as controlrequestaccess

	thingrestrictedobject <.. thingaccess
	actorcollectioncurator --- thingrepositoryrequestaccesscollectioncurator
	thingrepositoryrequestaccesscollectioncurator --> controlcontactrepositoryrequestaccesscollectioncurator
	controlcontactrepositoryrequestaccesscollectioncurator --> controlrequestaccess
	thingaccess --- controlrequestaccess
	thingrestrictedobject --- controlrequestaccess
	thingrepositoryrequestaccesscollectioncurator --> controlrequestaccess

@enduml