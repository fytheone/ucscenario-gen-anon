
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Descriptive ,\nExist Element" as thingdescriptiveexistingelement #grey
	entity "Collection" as thingcollection
	entity "Metadata Element" as thingmetadataelement
	actor "Collection Curator" as actorcollectioncurator
	boundary "Element\nInterface" as thingelementinterface #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	control "Select Any\nDescriptive, Existing\nElement" as controlselectdescriptiveexistingelementrequirecollection
	control "Be Optional" as controlbe
	control "Require For\nThat Collection" as controlrequirecollection
	control "Select Any\nDescriptive, Existing\nMetadata Element" as controlselectdescriptiveexistingmetadataelementbe

	actorcollectioncurator --- thingelementinterface
	actorcollectioncurator --- thingcollectioninterface
	thingdescriptiveexistingelement --- controlselectdescriptiveexistingelementrequirecollection
	thingelementinterface --> controlselectdescriptiveexistingelementrequirecollection
	controlselectdescriptiveexistingmetadataelementbe --> controlbe
	thingelementinterface --> controlbe
	controlselectdescriptiveexistingelementrequirecollection --> controlrequirecollection
	thingcollection --- controlrequirecollection
	thingcollectioninterface --> controlrequirecollection
	thingmetadataelement --- controlselectdescriptiveexistingmetadataelementbe
	thingelementinterface --> controlselectdescriptiveexistingmetadataelementbe

@enduml