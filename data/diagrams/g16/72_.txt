
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository ,\nSpecific Object" as thingrepositoryspecificobject
	entity "Target" as thingtarget
	actor "Collection Curator" as actorcollectioncurator
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Associate Targets\nWith Repository,\nSpecific Object" as controlassociatetarget

	thingrepositoryspecificobject <.. thingtarget
	actorcollectioncurator --- thingobjectinterface
	thingtarget --- controlassociatetarget
	thingrepositoryspecificobject --- controlassociatetarget
	thingobjectinterface --> controlassociatetarget

@enduml