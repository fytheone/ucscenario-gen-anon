
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Error Pages" as thingerrorpages
	actor "User" as actoruser
	boundary "Pages\nInterface" as thingpagesinterface #grey
	control "Have Nice\nError Pages" as controlhaveniceerrorpages

	actoruser --- thingpagesinterface
	thingerrorpages --- controlhaveniceerrorpages
	thingpagesinterface --> controlhaveniceerrorpages

@enduml