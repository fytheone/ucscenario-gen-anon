
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Session" as thingsession
	entity "Story" as thingstory
	actor "Moderator" as actormoderator
	boundary "Session\nInterface" as thingsessioninterface #grey
	boundary "Story\nInterface" as thingstoryinterface #grey
	control "Estimated In\nThe Session" as controlestimateestimatestorysession
	control "Estimate A\nStory" as controlestimatestory

	actormoderator --- thingsessioninterface
	actormoderator --- thingstoryinterface
	thingsession --- controlestimateestimatestorysession
	thingsessioninterface --> controlestimateestimatestorysession
	controlestimateestimatestorysession --> controlestimatestory
	thingstory --- controlestimatestory
	thingstoryinterface --> controlestimatestory

@enduml