
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Session" as thingsession
	entity "Item" as thingitem
	actor "Moderator" as actormoderator
	boundary "Session\nInterface" as thingsessioninterface #grey
	boundary "Item\nInterface" as thingiteminterface #grey
	control "Try" as controltryseeitemestimatesession
	control "See All\nItems" as controlseeitem
	control "Estimate This\nSession" as controlestimatesession

	actormoderator --- thingsessioninterface
	actormoderator --- thingiteminterface
	thingsessioninterface --> controltryseeitemestimatesession
	controltryseeitemestimatesession --> controlseeitem
	thingitem --- controlseeitem
	thingiteminterface --> controlseeitem
	controltryseeitemestimatesession --> controlestimatesession
	thingsession --- controlestimatesession
	thingsessioninterface --> controlestimatesession

@enduml