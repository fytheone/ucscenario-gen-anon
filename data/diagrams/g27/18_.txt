
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Altmetric" as thingaltmetric
	entity "Useful Statistic" as thingusefulstatistic #grey
	entity "Comprehensive" as thingcomprehensive
	entity "Repository" as thingrepository
	entity "Analytic" as thinganalytic
	entity "Item Level" as thingitemlevel #grey
	entity "Author" as thingauthor
	entity "Usage Statistics" as thingusagestatistics
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	boundary "Level\nInterface" as thinglevelinterface #grey
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	boundary "Analytic\nInterface" as thinganalyticinterface #grey
	boundary "Comprehensive\nInterface" as thingcomprehensiveinterface #grey
	boundary "Author\nInterface" as thingauthorinterface #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	control "Access Useful\nStatistics Including\nAltmetrics At\nItem Level" as controlaccessusefulstatisticitemlevel
	control "Access Useful\nUsage Statistics\nIncluding Altmetrics\nAt The\nRepository" as controlaccessusefulusagestatisticsrepository
	control "Access Analytic" as controlaccessanalytic
	control "Access Comprehensive" as controlaccesscomprehensive
	control "Access Useful\nStatistics Including\nAltmetrics At\nAuthor" as controlaccessusefulstatisticauthor
	control "Access Useful\nStatistics Including\nAltmetrics At\nCollection" as controlaccessusefulstatisticcollection

	thingrepository <.. thingaltmetric
	thingitemlevel <.. thingaltmetric
	thingcollection <.. thingaltmetric
	thingauthor <.. thingaltmetric
	thingaltmetric <.. thingusefulstatistic
	thingaltmetric <.. thingusagestatistics
	actorlibrarystaffmember --- thinglevelinterface
	actorlibrarystaffmember --- thingrepositoryinterface
	actorlibrarystaffmember --- thinganalyticinterface
	actorlibrarystaffmember --- thingcomprehensiveinterface
	actorlibrarystaffmember --- thingauthorinterface
	actorlibrarystaffmember --- thingcollectioninterface
	thingitemlevel --- controlaccessusefulstatisticitemlevel
	thinglevelinterface --> controlaccessusefulstatisticitemlevel
	thingusefulstatistic --- controlaccessusefulstatisticitemlevel
	thingaltmetric --- controlaccessusefulstatisticitemlevel
	thingusagestatistics --- controlaccessusefulusagestatisticsrepository
	thingaltmetric --- controlaccessusefulusagestatisticsrepository
	thingrepository --- controlaccessusefulusagestatisticsrepository
	thingrepositoryinterface --> controlaccessusefulusagestatisticsrepository
	thinganalytic --- controlaccessanalytic
	thinganalyticinterface --> controlaccessanalytic
	thingcomprehensive --- controlaccesscomprehensive
	thingcomprehensiveinterface --> controlaccesscomprehensive
	thingusefulstatistic --- controlaccessusefulstatisticauthor
	thingauthor --- controlaccessusefulstatisticauthor
	thingauthorinterface --> controlaccessusefulstatisticauthor
	thingaltmetric --- controlaccessusefulstatisticauthor
	thingcollection --- controlaccessusefulstatisticcollection
	thingcollectioninterface --> controlaccessusefulstatisticcollection
	thingusefulstatistic --- controlaccessusefulstatisticcollection
	thingaltmetric --- controlaccessusefulstatisticcollection

@enduml