
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Faculty" as thingfaculty
	entity "Digestible Way" as thingdigestibleway
	entity "Tracking Statistics" as thingtrackingstatistics
	actor "Administrator" as actoradministrator
	boundary "Way\nInterface" as thingwayinterface #grey
	control "See Comprehensive\nTracking Statistics\nPresented To\nFaculty In\nA Digestible\nWay" as controlseecomprehensivetrackingstatistics

	thingdigestibleway <.. thingfaculty
	thingfaculty <.. thingtrackingstatistics
	thingdigestibleway <.. thingtrackingstatistics
	actoradministrator --- thingwayinterface
	thingtrackingstatistics --- controlseecomprehensivetrackingstatistics
	thingfaculty --- controlseecomprehensivetrackingstatistics
	thingdigestibleway --- controlseecomprehensivetrackingstatistics
	thingwayinterface --> controlseecomprehensivetrackingstatistics

@enduml