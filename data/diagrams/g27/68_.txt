
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository Datum" as thingrepositorydatum
	entity "Embargoed Dissertation" as thingembargoeddissertation
	entity "Dmp" as thingDMP
	entity "Nsf Ddig" as thingnsfddig
	actor "Student" as actorstudent
	circle "Requirement" as thingrequirement
	boundary "Ddig\nInterface" as thingddiginterface #grey
	control "Upload To\nThe The\nRepository Data\nAssociated Embargoed\nDissertation To\nMeet Requirements\nOf A\nDmp" as controluploadrepositorydatum
	control "Submitted With\nNsf Ddig" as controlsubmituploadrepositorydatumnsfDDIG

	thingembargoeddissertation <.. thingrepositorydatum
	thingrequirement <.. thingrepositorydatum
	thingrequirement <.. thingembargoeddissertation
	thingDMP *-- thingrequirement
	actorstudent --- thingddiginterface
	thingrepositorydatum --- controluploadrepositorydatum
	thingembargoeddissertation --- controluploadrepositorydatum
	thingrequirement --- controluploadrepositorydatum
	thingDMP --- controluploadrepositorydatum
	controlsubmituploadrepositorydatumnsfDDIG --> controluploadrepositorydatum
	thingddiginterface --> controluploadrepositorydatum
	thingnsfddig --- controlsubmituploadrepositorydatumnsfDDIG
	thingddiginterface --> controlsubmituploadrepositorydatumnsfDDIG

@enduml