
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Rare" as thingrare
	entity "Archival" as thingarchival
	entity "Endanger Material" as thingendangeredmaterial
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	boundary "Archival\nInterface" as thingarchivalinterface #grey
	boundary "Rare\nInterface" as thingrareinterface #grey
	boundary "Material\nInterface" as thingmaterialinterface #grey
	control "Create Collections\nHighlight Archival" as controlcreatecollectionarchival
	control "Create Collections\nHighlight Rare" as controlcreatecollection
	control "Create Collections\nHighlight Endangered\nMaterial" as controlcreatecollectionendangeredmaterial

	thingrare <.. thingcollection
	thingendangeredmaterial <.. thingcollection
	thingarchival <.. thingcollection
	actorlibrarystaffmember --- thingarchivalinterface
	actorlibrarystaffmember --- thingrareinterface
	actorlibrarystaffmember --- thingmaterialinterface
	thingarchival --- controlcreatecollectionarchival
	thingarchivalinterface --> controlcreatecollectionarchival
	thingcollection --- controlcreatecollectionarchival
	thingcollection --- controlcreatecollection
	thingrare --- controlcreatecollection
	thingrareinterface --> controlcreatecollection
	thingendangeredmaterial --- controlcreatecollectionendangeredmaterial
	thingmaterialinterface --> controlcreatecollectionendangeredmaterial
	thingcollection --- controlcreatecollectionendangeredmaterial

@enduml