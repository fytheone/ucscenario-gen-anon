
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Content" as thingcontent
	entity "Relevant" as thingrelevant
	entity "Suitable ,\nTimely Research" as thingsuitabletimelyresearch
	entity "Use" as thinguse
	entity "Curriculum" as thingcurriculum
	entity "New Research" as thingnewresearch
	actor "Teachingfacultymember" as actorteachingfacultymember
	boundary "Content\nInterface" as thingcontentinterface #grey
	boundary "Research\nInterface" as thingresearchinterface #grey
	boundary "Relevant\nInterface" as thingrelevantinterface #grey
	control "Notified Identify\nSuitable, Timely\nResearch For\nUse In\nCurriculum Of\nContent" as controlidentifysuitabletimelyresearchcontent
	control "Notified Identify\nSuitable, Timely\nResearch For\nUse In\nCurriculum Of\nNew Research" as controlidentifysuitabletimelyresearchnewresearch
	control "Identify Relevant" as controlidentifyrelevant

	thinguse <.. thingsuitabletimelyresearch
	thingcurriculum <.. thinguse
	thingnewresearch <.. thingcurriculum
	thingcontent <.. thingcurriculum
	actorteachingfacultymember --- thingcontentinterface
	actorteachingfacultymember --- thingresearchinterface
	actorteachingfacultymember --- thingrelevantinterface
	thingcontent --- controlidentifysuitabletimelyresearchcontent
	thingcontentinterface --> controlidentifysuitabletimelyresearchcontent
	thingsuitabletimelyresearch --- controlidentifysuitabletimelyresearchcontent
	thinguse --- controlidentifysuitabletimelyresearchcontent
	thingcurriculum --- controlidentifysuitabletimelyresearchcontent
	thingsuitabletimelyresearch --- controlidentifysuitabletimelyresearchnewresearch
	thinguse --- controlidentifysuitabletimelyresearchnewresearch
	thingcurriculum --- controlidentifysuitabletimelyresearchnewresearch
	thingnewresearch --- controlidentifysuitabletimelyresearchnewresearch
	thingresearchinterface --> controlidentifysuitabletimelyresearchnewresearch
	thingrelevant --- controlidentifyrelevant
	thingrelevantinterface --> controlidentifyrelevant

@enduml