
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Asset" as thingasset
	entity "Mechanism" as thingmechanism
	entity "Successor" as thingsuccessor
	entity "Management" as thingmanagement
	actor "Asset Manager" as actorassetmanager
	circle "Ownership" as thingownership
	boundary "Management\nInterface" as thingmanagementinterface #grey
	boundary "Mechanism\nInterface" as thingmechanisminterface #grey
	control "Have Mechanism" as controlhavemechanismpasspassmanagement
	control "Pass Management" as controlpassmanagement
	control "Pass Ownership\nOf The\nAssets To\nA Successor" as controlpassownershipsuccessor

	thingsuccessor <.. thingasset
	thingasset *-- thingownership
	actorassetmanager --- thingmanagementinterface
	actorassetmanager --- thingmechanisminterface
	thingmechanism --- controlhavemechanismpasspassmanagement
	thingmechanisminterface --> controlhavemechanismpasspassmanagement
	controlhavemechanismpasspassmanagement --> controlpassmanagement
	thingmanagement --- controlpassmanagement
	thingmanagementinterface --> controlpassmanagement
	thingownership --- controlpassownershipsuccessor
	thingasset --- controlpassownershipsuccessor
	thingsuccessor --- controlpassownershipsuccessor
	thingmechanisminterface --> controlpassownershipsuccessor
	controlhavemechanismpasspassmanagement --> controlpassownershipsuccessor

@enduml