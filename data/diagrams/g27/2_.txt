
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	entity "Repository" as thingrepository
	entity "Publication" as thingpublication
	actor "Cornell ,\nFaculty Member" as actorcornellfacultymember
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	boundary "Publication\nInterface" as thingpublicationinterface #grey
	control "Upload Data\nTo The\nRepository" as controluploaddatumrepository
	control "Associate The\nRepository With\nA Publication" as controlassociaterepositorypublication

	thingrepository <.. thingdatum
	thingpublication <.. thingrepository
	actorcornellfacultymember --- thingrepositoryinterface
	actorcornellfacultymember --- thingpublicationinterface
	thingdatum --- controluploaddatumrepository
	thingrepository --- controluploaddatumrepository
	thingrepositoryinterface --> controluploaddatumrepository
	thingpublication --- controlassociaterepositorypublication
	thingpublicationinterface --> controlassociaterepositorypublication
	thingrepository --- controlassociaterepositorypublication

@enduml