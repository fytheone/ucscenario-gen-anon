
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datapackage" as thingDataPackage
	entity "Datum" as thingdatum
	entity "Sqlite Database" as thingsqlitedatabase
	actor "Consumer" as actorconsumer
	boundary "Database\nInterface" as thingdatabaseinterface #grey
	control "Download A\nDatapackage's Data\nCoherent Sqlite\nDatabase" as controldownloaddatumcoherentsqlitedatabase

	thingDataPackage <.. thingdatum
	thingsqlitedatabase <.. thingdatum
	actorconsumer --- thingdatabaseinterface
	thingdatum --- controldownloaddatumcoherentsqlitedatabase
	thingsqlitedatabase --- controldownloaddatumcoherentsqlitedatabase
	thingdatabaseinterface --> controldownloaddatumcoherentsqlitedatabase

@enduml