
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Profile" as thingprofile
	entity "Publisher" as thingpublisher
	actor "Consumer" as actorconsumer
	boundary "Profile\nInterface" as thingprofileinterface #grey
	control "See A\nPublisher's Profile" as controlseeprofile

	thingpublisher <.. thingprofile
	actorconsumer --- thingprofileinterface
	thingprofile --- controlseeprofile
	thingprofileinterface --> controlseeprofile

@enduml