
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Project" as thingproject
	entity "Administrative Information" as thingadministrativeinformation
	entity "Dmp" as thingDMP
	actor "Datum Librarian" as actordatalibrarian
	boundary "Dmp\nInterface" as thingdmpinterface #grey
	control "Import Administrative\nInformation Regarding\nA Project\nInto Dmp" as controlimportadministrativeinformation

	thingDMP <.. thingproject
	thingproject <.. thingadministrativeinformation
	actordatalibrarian --- thingdmpinterface
	thingadministrativeinformation --- controlimportadministrativeinformation
	thingproject --- controlimportadministrativeinformation
	thingDMP --- controlimportadministrativeinformation
	thingdmpinterface --> controlimportadministrativeinformation

@enduml