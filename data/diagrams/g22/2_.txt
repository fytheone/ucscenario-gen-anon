
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Metadata" as thingmetadata
	actor "Researcher" as actorresearcher
	circle "Tool" as thingtool #grey
	boundary "Metadata\nInterface" as thingmetadatainterface #grey
	control "That Captured\nWith A\nMetadata Tool" as controlcaptureimportmetadatathatmetadatatool
	control "Import Metadata" as controlimportmetadata

	thingmetadata *-- thingtool
	actorresearcher --- thingmetadatainterface
	thingtool --- controlcaptureimportmetadatathatmetadatatool
	thingmetadatainterface --> controlcaptureimportmetadatathatmetadatatool
	controlcaptureimportmetadatathatmetadatatool --> controlimportmetadata
	thingmetadata --- controlimportmetadata
	thingmetadatainterface --> controlimportmetadata

@enduml