
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Github" as thingGitHub
	entity "Data Package" as thingdatapackage #grey
	entity "Ckan" as thingCKAN
	actor "Developerdatawrangler" as actorDeveloperdataWrangler
	boundary "Github\nInterface" as thinggithubinterface #grey
	control "Store Data\nPackage In\nGithub" as controlstoredatapackageGitHub
	control "Have" as controlhave

	thingGitHub <.. thingdatapackage
	actorDeveloperdataWrangler --- thinggithubinterface
	thingdatapackage --- controlstoredatapackageGitHub
	thingGitHub --- controlstoredatapackageGitHub
	thinggithubinterface --> controlstoredatapackageGitHub

@enduml