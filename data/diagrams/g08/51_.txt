
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Site" as thingsite
	entity "Preview Button" as thingpreviewbutton
	actor "Publisher" as actorpublisher
	boundary "Site\nInterface" as thingsiteinterface #grey
	control "Embed A\nPreview Button\nOn Site" as controlembedpreviewbuttonsite

	thingsite <.. thingpreviewbutton
	actorpublisher --- thingsiteinterface
	thingpreviewbutton --- controlembedpreviewbuttonsite
	thingsite --- controlembedpreviewbuttonsite
	thingsiteinterface --> controlembedpreviewbuttonsite

@enduml