
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Second" as thingsecond
	entity "Julia" as thingjulia
	entity "Data Package" as thingdatapackage #grey
	actor "Developer" as actordeveloper
	boundary "Second\nInterface" as thingsecondinterface #grey
	control "Get A\nData Package\nInto Julia\nIn Seconds" as controlgetdatapackagejuliasecond

	thingsecond <.. thingjulia
	thingjulia <.. thingdatapackage
	actordeveloper --- thingsecondinterface
	thingdatapackage --- controlgetdatapackagejuliasecond
	thingjulia --- controlgetdatapackagejuliasecond
	thingsecond --- controlgetdatapackagejuliasecond
	thingsecondinterface --> controlgetdatapackagejuliasecond

@enduml