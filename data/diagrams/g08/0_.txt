
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Package" as thingdatapackage #grey
	entity "Node" as thingnode
	actor "Developer" as actordeveloper
	boundary "Node\nInterface" as thingnodeinterface #grey
	control "Get A\nData Package\nInto Node" as controlgetdatapackagenode

	thingnode <.. thingdatapackage
	actordeveloper --- thingnodeinterface
	thingdatapackage --- controlgetdatapackagenode
	thingnode --- controlgetdatapackagenode
	thingnodeinterface --> controlgetdatapackagenode

@enduml