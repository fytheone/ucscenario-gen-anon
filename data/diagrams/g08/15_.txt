
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Excel Spreadsheet" as thingexcelspreadsheet #grey
	entity "Data Package" as thingdatapackage #grey
	actor "Researcher" as actorresearcher
	boundary "Package\nInterface" as thingpackageinterface #grey
	control "Get Excel\nSpreadsheet Into\nA Data\nPackage" as controlgetexcelspreadsheetdatapackage

	thingdatapackage <.. thingexcelspreadsheet
	actorresearcher --- thingpackageinterface
	thingexcelspreadsheet --- controlgetexcelspreadsheetdatapackage
	thingdatapackage --- controlgetexcelspreadsheetdatapackage
	thingpackageinterface --> controlgetexcelspreadsheetdatapackage

@enduml