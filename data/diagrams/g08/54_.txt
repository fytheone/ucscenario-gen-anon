
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Useful Metadata" as thingusefulmetadata
	entity "Dataset" as thingdataset
	entity "Data Columns" as thingdatacolumns
	actor "Publisher" as actorpublisher
	boundary "Metadata\nInterface" as thingmetadatainterface #grey
	boundary "Columns\nInterface" as thingcolumnsinterface #grey
	boundary "Dataset\nInterface" as thingdatasetinterface #grey
	control "Add Useful\nMetadata" as controladdusefulmetadata
	control "Add In\nNew Data\nColumns" as controladdnewdatacolumnsmakedataset
	control "Make The\nDataset" as controlmakedataset

	actorpublisher --- thingmetadatainterface
	actorpublisher --- thingcolumnsinterface
	actorpublisher --- thingdatasetinterface
	thingusefulmetadata --- controladdusefulmetadata
	thingmetadatainterface --> controladdusefulmetadata
	thingdatacolumns --- controladdnewdatacolumnsmakedataset
	thingcolumnsinterface --> controladdnewdatacolumnsmakedataset
	controladdnewdatacolumnsmakedataset --> controlmakedataset
	thingdataset --- controlmakedataset
	thingdatasetinterface --> controlmakedataset

@enduml