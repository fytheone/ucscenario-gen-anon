
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdata
	actor "Publisher" as actorpublisher
	boundary "Data\nInterface" as thingdatainterface #grey
	control "Online Store\nData" as controlstoredata

	actorpublisher --- thingdatainterface
	thingdata --- controlstoredata
	thingdatainterface --> controlstoredata

@enduml