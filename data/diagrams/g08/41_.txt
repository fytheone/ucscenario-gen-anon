
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Integration" as thingintegration
	entity "Excel" as thingexcel
	actor "Publisher" as actorpublisher
	boundary "Excel\nInterface" as thingexcelinterface #grey
	control "Provided With\nAn Integration\nWith Excel" as controlprovideintegration

	thingexcel <.. thingintegration
	actorpublisher --- thingexcelinterface
	thingintegration --- controlprovideintegration
	thingexcel --- controlprovideintegration
	thingexcelinterface --> controlprovideintegration

@enduml