
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Search" as thingsearch
	entity "Occupation" as thingoccupation
	entity "Collection" as thingcollection
	entity ", Pertinent\nHeading" as thingsuchpertinentheading
	entity "Title" as thingtitle
	entity "Topic" as thingtopic
	entity "Etc" as thingetc
	entity "Object" as thingobject
	entity "Genre" as thinggenre
	entity "Cartographic" as thingcartographic
	entity "Function" as thingfunction
	entity "Form" as thingform
	actor "User" as actoruser
	circle "Component" as thingcomponent #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Occupation\nInterface" as thingoccupationinterface #grey
	boundary "Title\nInterface" as thingtitleinterface #grey
	boundary "Topic\nInterface" as thingtopicinterface #grey
	boundary "Heading\nInterface" as thingheadinginterface #grey
	boundary "Form\nInterface" as thingforminterface #grey
	boundary "Cartographic\nInterface" as thingcartographicinterface #grey
	boundary "Function\nInterface" as thingfunctioninterface #grey
	boundary "Etc\nInterface" as thingetcinterface #grey
	boundary "Genre\nInterface" as thinggenreinterface #grey
	control "Provided With\nSearch For\nObject Component" as controlprovidesearchobjectcomponent
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nOccupation" as controlprovidesearchoccupation
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nTitle" as controlprovidesearch
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nTopic" as controlprovidesearchtopic
	control "Provided With\nSearch For\nObject" as controlprovidesearchobject
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nName" as controlprovidesearchname
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nForm" as controlprovidesearchform
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nCartographic" as controlprovidesearchcartographic
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nFunction" as controlprovidesearchfunction
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nGeographical Name" as controlprovidesearchgeographicalname
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nEtc" as controlprovidesearchetc
	control "Provided With\nSearch For\nCollections By\nSuch, Pertinent\nHeadings Including\nGenre" as controlprovidesearchgenre

	thingcollection <.. thingsearch
	thingsuchpertinentheading <.. thingsearch
	thingcomponent <.. thingsearch
	thingobject <.. thingsearch
	thingsuchpertinentheading <.. thingcollection
	thingtitle <.. thingsuchpertinentheading
	thingfunction <.. thingsuchpertinentheading
	thinggenre <.. thingsuchpertinentheading
	thingoccupation <.. thingsuchpertinentheading
	thingcartographic <.. thingsuchpertinentheading
	thingetc <.. thingsuchpertinentheading
	thingform <.. thingsuchpertinentheading
	thingtopic <.. thingsuchpertinentheading
	thingobject *-- thingcomponent
	actoruser --- thingobjectinterface
	actoruser --- thingoccupationinterface
	actoruser --- thingtitleinterface
	actoruser --- thingtopicinterface
	actoruser --- thingheadinginterface
	actoruser --- thingforminterface
	actoruser --- thingcartographicinterface
	actoruser --- thingfunctioninterface
	actoruser --- thingetcinterface
	actoruser --- thinggenreinterface
	thingcomponent --- controlprovidesearchobjectcomponent
	thingobjectinterface --> controlprovidesearchobjectcomponent
	thingsearch --- controlprovidesearchobjectcomponent
	thingoccupation --- controlprovidesearchoccupation
	thingoccupationinterface --> controlprovidesearchoccupation
	thingsuchpertinentheading --- controlprovidesearchoccupation
	thingsearch --- controlprovidesearchoccupation
	thingcollection --- controlprovidesearchoccupation
	thingsearch --- controlprovidesearch
	thingcollection --- controlprovidesearch
	thingsuchpertinentheading --- controlprovidesearch
	thingtitle --- controlprovidesearch
	thingtitleinterface --> controlprovidesearch
	thingtopic --- controlprovidesearchtopic
	thingtopicinterface --> controlprovidesearchtopic
	thingsuchpertinentheading --- controlprovidesearchtopic
	thingsearch --- controlprovidesearchtopic
	thingcollection --- controlprovidesearchtopic
	thingobject --- controlprovidesearchobject
	thingobjectinterface --> controlprovidesearchobject
	thingsearch --- controlprovidesearchobject
	thingheadinginterface --> controlprovidesearchname
	thingsuchpertinentheading --- controlprovidesearchname
	thingsearch --- controlprovidesearchname
	thingcollection --- controlprovidesearchname
	thingform --- controlprovidesearchform
	thingforminterface --> controlprovidesearchform
	thingsuchpertinentheading --- controlprovidesearchform
	thingsearch --- controlprovidesearchform
	thingcollection --- controlprovidesearchform
	thingcartographic --- controlprovidesearchcartographic
	thingcartographicinterface --> controlprovidesearchcartographic
	thingsuchpertinentheading --- controlprovidesearchcartographic
	thingsearch --- controlprovidesearchcartographic
	thingcollection --- controlprovidesearchcartographic
	thingfunction --- controlprovidesearchfunction
	thingfunctioninterface --> controlprovidesearchfunction
	thingsuchpertinentheading --- controlprovidesearchfunction
	thingsearch --- controlprovidesearchfunction
	thingcollection --- controlprovidesearchfunction
	thingsuchpertinentheading --- controlprovidesearchgeographicalname
	thingheadinginterface --> controlprovidesearchgeographicalname
	thingsearch --- controlprovidesearchgeographicalname
	thingcollection --- controlprovidesearchgeographicalname
	thingetc --- controlprovidesearchetc
	thingetcinterface --> controlprovidesearchetc
	thingsuchpertinentheading --- controlprovidesearchetc
	thingsearch --- controlprovidesearchetc
	thingcollection --- controlprovidesearchetc
	thinggenre --- controlprovidesearchgenre
	thinggenreinterface --> controlprovidesearchgenre
	thingsuchpertinentheading --- controlprovidesearchgenre
	thingsearch --- controlprovidesearchgenre
	thingcollection --- controlprovidesearchgenre

@enduml