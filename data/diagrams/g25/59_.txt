
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Title" as thingtitle
	entity "Object" as thingobject
	actor "User" as actoruser
	circle "Component" as thingcomponent #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Title\nInterface" as thingtitleinterface #grey
	control "Search Collections" as controlsearchcollection
	control "Search Object\nComponent" as controlsearchobjectcomponent
	control "Search Object\nBy Title" as controlsearchobject

	thingtitle <.. thingobject
	thingobject *-- thingcomponent
	actoruser --- thingcollectioninterface
	actoruser --- thingobjectinterface
	actoruser --- thingtitleinterface
	thingcollection --- controlsearchcollection
	thingcollectioninterface --> controlsearchcollection
	thingcomponent --- controlsearchobjectcomponent
	thingobjectinterface --> controlsearchobjectcomponent
	thingobject --- controlsearchobject
	thingtitle --- controlsearchobject
	thingtitleinterface --> controlsearchobject

@enduml