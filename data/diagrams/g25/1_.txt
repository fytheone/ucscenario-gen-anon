
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository Information" as thingrepositoryinformation
	entity "Place" as thingplace
	entity "Relevant Place" as thingrelevantplace
	actor "Repository Manager" as actorrepositorymanager
	boundary "Place\nInterface" as thingplaceinterface #grey
	control "Update Repository\nInformation In\nPlace" as controlupdaterepositoryinformationplace
	control "Propagated To\nAll Relevant\nPlaces" as controlpropagaterelevantplace

	thingplace <.. thingrepositoryinformation
	actorrepositorymanager --- thingplaceinterface
	thingrepositoryinformation --- controlupdaterepositoryinformationplace
	thingplace --- controlupdaterepositoryinformationplace
	thingplaceinterface --> controlupdaterepositoryinformationplace
	thingrelevantplace --- controlpropagaterelevantplace
	thingplaceinterface --> controlpropagaterelevantplace

@enduml