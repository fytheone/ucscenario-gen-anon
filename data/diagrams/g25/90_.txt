
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Way" as thingway
	entity "Access" as thingaccess
	entity "Library Policy" as thinglibrarypolicy #grey
	actor "Dams Manager" as actordamsmanager
	circle "Component" as thingcomponent #grey
	boundary "Way\nInterface" as thingwayinterface #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Know An\nObject Subject\nTo A\nLibrary Policy\nRestricts Access\nIn Any\nWay" as controlknowobjectlibrarypolicy
	control "Know Object\nComponent" as controlknowobjectcomponent

	thinglibrarypolicy <.. thingobject
	thingway <.. thingaccess
	thingaccess <.. thinglibrarypolicy
	thingway <.. thinglibrarypolicy
	thingobject *-- thingcomponent
	actordamsmanager --- thingwayinterface
	actordamsmanager --- thingobjectinterface
	thingobject --- controlknowobjectlibrarypolicy
	thinglibrarypolicy --- controlknowobjectlibrarypolicy
	thingaccess --- controlknowobjectlibrarypolicy
	thingway --- controlknowobjectlibrarypolicy
	thingwayinterface --> controlknowobjectlibrarypolicy
	thingcomponent --- controlknowobjectcomponent
	thingobjectinterface --> controlknowobjectcomponent

@enduml