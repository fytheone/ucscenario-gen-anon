
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "News Items" as thingnewsitems #grey
	entity "Priority Numbers" as thingprioritynumbers #grey
	actor "Site Editor" as actorsiteeditor
	boundary "Items\nInterface" as thingitemsinterface #grey
	control "Assign Priority\nNumbers To\nNews Items" as controlassignprioritynumbersnewsitems

	thingnewsitems <.. thingprioritynumbers
	actorsiteeditor --- thingitemsinterface
	thingprioritynumbers --- controlassignprioritynumbersnewsitems
	thingnewsitems --- controlassignprioritynumbersnewsitems
	thingitemsinterface --> controlassignprioritynumbersnewsitems

@enduml