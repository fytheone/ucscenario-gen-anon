
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Application" as thingapplication
	actor "Site Member" as actorsitemember
	boundary "Application\nInterface" as thingapplicationinterface #grey
	control "Fill Out\nAn To\nApplication A\nCertified Scrum\nTrainer" as controlfillapplication

	actorsitemember --- thingapplicationinterface
	thingapplication --- controlfillapplication
	thingapplicationinterface --> controlfillapplication

@enduml