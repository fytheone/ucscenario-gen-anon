
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Upcome Course" as thingupcomingcourse
	actor "Trainer" as actortrainer
	boundary "Visitor" as thingvisitortrainer
	control "What Notice" as controlnoticewhatvisitortrainer
	control "See" as controlsee

	actortrainer --- thingvisitortrainer
	thingvisitortrainer --> controlnoticewhatvisitortrainer

@enduml