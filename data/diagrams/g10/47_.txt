
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Job" as thingjob
	entity "Site" as thingsite
	actor "Site Admin" as actorsiteadmin
	boundary "Site\nInterface" as thingsiteinterface #grey
	control "Publishing Jobs\nOn The\nSite" as controlpublishjob
	control "Days Stop\nPosted" as controlstoppublishjob

	thingsite <.. thingjob
	actorsiteadmin --- thingsiteinterface
	thingjob --- controlpublishjob
	thingsite --- controlpublishjob
	controlstoppublishjob --> controlpublishjob
	thingsiteinterface --> controlpublishjob
	thingsiteinterface --> controlstoppublishjob

@enduml