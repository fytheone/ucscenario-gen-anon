
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ad" as thingad
	actor "Recruiter" as actorrecruiter
	boundary "Help" as thinghelpwantadrecruiter #grey
	control "Want Ad" as controlwantad
	control "Post" as controlposthelpwantadrecruiter

	actorrecruiter --- thinghelpwantadrecruiter
	thingad --- controlwantad
	controlposthelpwantadrecruiter --> controlwantad
	thinghelpwantadrecruiter --> controlwantad
	thinghelpwantadrecruiter --> controlposthelpwantadrecruiter

@enduml