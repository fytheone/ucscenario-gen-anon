
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Basic" as thingbasic
	entity "Website" as thingwebsite
	actor "Site Visitor" as actorsitevisitor
	circle "Section" as thingsection
	boundary "Basic\nInterface" as thingbasicinterface #grey
	control "View A\nSection Of\nThe Website\nTeaches Me\nThe Basics\nWhat Scrum\nIs" as controlviewsection

	thingbasic <.. thingwebsite
	thingwebsite *-- thingsection
	actorsitevisitor --- thingbasicinterface
	thingsection --- controlviewsection
	thingwebsite --- controlviewsection
	thingbasic --- controlviewsection
	thingbasicinterface --> controlviewsection

@enduml