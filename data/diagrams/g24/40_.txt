
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Cris" as thingCRIS
	entity "Archive" as thingarchive
	actor "Research ,\nInformation Manager" as actorresearchinformationmanager
	boundary "Cris\nInterface" as thingcrisinterface #grey
	control "Integrate The\nArchive With\nCris" as controlintegratearchive

	thingCRIS <.. thingarchive
	actorresearchinformationmanager --- thingcrisinterface
	thingarchive --- controlintegratearchive
	thingCRIS --- controlintegratearchive
	thingcrisinterface --> controlintegratearchive

@enduml