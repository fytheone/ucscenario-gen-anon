
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Opus" as thingopus
	entity "Dataset" as thingdataset
	entity "Publication" as thingpublication
	actor "Depositor" as actordepositor
	boundary "Opus\nInterface" as thingopusinterface #grey
	control "Link Datasets\nTo Publications\nIn Opus" as controllinkdatasetpublicationopus

	thingpublication <.. thingdataset
	thingopus <.. thingpublication
	actordepositor --- thingopusinterface
	thingdataset --- controllinkdatasetpublicationopus
	thingpublication --- controllinkdatasetpublicationopus
	thingopus --- controllinkdatasetpublicationopus
	thingopusinterface --> controllinkdatasetpublicationopus

@enduml