
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Previous Deposit" as thingpreviousdeposit
	entity "Metadata" as thingmetadata
	entity "University Systems" as thinguniversitysystems
	actor "Depositor" as actordepositor
	boundary "Deposit\nInterface" as thingdepositinterface #grey
	control "Remembered Have\nAutomatically Metadata\nFrom Other\nUniversity Systems\nFrom Previous\nDeposits" as controlhavemetadatapreviousdeposit

	thinguniversitysystems <.. thingmetadata
	thingpreviousdeposit <.. thinguniversitysystems
	actordepositor --- thingdepositinterface
	thingmetadata --- controlhavemetadatapreviousdeposit
	thinguniversitysystems --- controlhavemetadatapreviousdeposit
	thingpreviousdeposit --- controlhavemetadatapreviousdeposit
	thingdepositinterface --> controlhavemetadatapreviousdeposit

@enduml