
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset" as thingdataset
	entity "Collaborator" as thingcollaborator
	entity "Access" as thingaccess
	actor "Depositor" as actordepositor
	control "Allow" as controlallow

	thingdataset <.. thingaccess

@enduml