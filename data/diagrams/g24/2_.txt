
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Pure" as thingpure
	entity "Dataset" as thingdataset
	actor "Depositor" as actordepositor
	boundary "Pure\nInterface" as thingpureinterface #grey
	control "Deposit Datasets\nThrough Pure" as controldepositdatasetpure
	control "Maintain Datasets\nThrough Pure" as controlmaintaindatasetpure

	thingpure <.. thingdataset
	actordepositor --- thingpureinterface
	thingdataset --- controldepositdatasetpure
	thingpure --- controldepositdatasetpure
	thingpureinterface --> controldepositdatasetpure
	thingpure --- controlmaintaindatasetpure
	thingdataset --- controlmaintaindatasetpure
	thingpureinterface --> controlmaintaindatasetpure

@enduml