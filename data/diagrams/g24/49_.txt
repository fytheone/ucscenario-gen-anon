
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Api" as thingsuchapi
	entity "Dataset" as thingdataset
	actor "Developer" as actordeveloper
	boundary "Api\nInterface" as thingapiinterface #grey
	control "Deposit Datasets\nVia An\nSuch Api" as controldepositdatasetsuchapi
	control "Maintain Datasets\nVia An\nSuch Api" as controlmaintaindatasetsuchapi

	thingsuchapi <.. thingdataset
	actordeveloper --- thingapiinterface
	thingdataset --- controldepositdatasetsuchapi
	thingsuchapi --- controldepositdatasetsuchapi
	thingapiinterface --> controldepositdatasetsuchapi
	thingsuchapi --- controlmaintaindatasetsuchapi
	thingdataset --- controlmaintaindatasetsuchapi
	thingapiinterface --> controlmaintaindatasetsuchapi

@enduml