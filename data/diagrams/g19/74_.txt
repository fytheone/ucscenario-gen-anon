
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Meeting" as thingmeeting
	entity "Alfred" as thingALFRED
	entity "Certain Place" as thingcertainplace
	entity "Friend" as thingfriend
	actor "Olderperson" as actorOlderPerson
	circle "Group" as thinggroup
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Place\nInterface" as thingplaceinterface #grey
	control "Use Alfred" as controluseALFREDorganizemeetingcertainplace
	control "Organize A\nMeeting With\nA Group\nOf Friends\nAt A\nCertain Place" as controlorganizemeetingcertainplace

	thinggroup <.. thingmeeting
	thingcertainplace <.. thingfriend
	thingfriend *-- thinggroup
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingplaceinterface
	thingALFRED --- controluseALFREDorganizemeetingcertainplace
	thingalfredinterface --> controluseALFREDorganizemeetingcertainplace
	controluseALFREDorganizemeetingcertainplace --> controlorganizemeetingcertainplace
	thingmeeting --- controlorganizemeetingcertainplace
	thinggroup --- controlorganizemeetingcertainplace
	thingfriend --- controlorganizemeetingcertainplace
	thingcertainplace --- controlorganizemeetingcertainplace
	thingplaceinterface --> controlorganizemeetingcertainplace

@enduml