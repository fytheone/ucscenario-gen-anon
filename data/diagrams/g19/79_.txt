
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Caregiver" as thingcaregiver
	entity "Button" as thingbutton
	actor "Olderperson" as actorOlderPerson
	boundary "Button\nInterface" as thingbuttoninterface #grey
	control "Call Caregiver" as controlcallcaregiver
	control "Press Button" as controlpressbuttoncallcaregiver

	actorOlderPerson --- thingbuttoninterface
	thingcaregiver --- controlcallcaregiver
	controlpressbuttoncallcaregiver --> controlcallcaregiver
	thingbuttoninterface --> controlcallcaregiver
	thingbutton --- controlpressbuttoncallcaregiver
	thingbuttoninterface --> controlpressbuttoncallcaregiver

@enduml