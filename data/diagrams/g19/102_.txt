
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Health Impairments" as thinghealthimpairments #grey
	entity "Medication" as thingmedication
	entity "Specific Tip" as thingspecifictip
	actor "Olderperson" as actorOlderPerson
	boundary "Tip\nInterface" as thingtipinterface #grey
	boundary "Medication\nInterface" as thingmedicationinterface #grey
	control "Receive Specific\nTips" as controlreceivespecifictipreducehealthimpairmentsmedication
	control "Reduce Health\nImpairments Without\nMedication" as controlreducehealthimpairmentsmedication

	thingmedication <.. thinghealthimpairments
	actorOlderPerson --- thingtipinterface
	actorOlderPerson --- thingmedicationinterface
	thingspecifictip --- controlreceivespecifictipreducehealthimpairmentsmedication
	thingtipinterface --> controlreceivespecifictipreducehealthimpairmentsmedication
	controlreceivespecifictipreducehealthimpairmentsmedication --> controlreducehealthimpairmentsmedication
	thinghealthimpairments --- controlreducehealthimpairmentsmedication
	thingmedication --- controlreducehealthimpairmentsmedication
	thingmedicationinterface --> controlreducehealthimpairmentsmedication

@enduml