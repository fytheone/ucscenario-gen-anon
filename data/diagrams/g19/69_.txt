
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Art Expositions" as thingartexpositions #grey
	entity "Museum" as thingmuseum
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Expositions\nInterface" as thingexpositionsinterface #grey
	control "Learn About\nA Museum" as controllearnmuseum
	control "Use Alfred" as controluseALFREDlearnartexpositions
	control "Learn About\nArt Expositions" as controllearnartexpositions

	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingexpositionsinterface
	thingmuseum --- controllearnmuseum
	thingalfredinterface --> controllearnmuseum
	controluseALFREDlearnartexpositions --> controllearnmuseum
	thingALFRED --- controluseALFREDlearnartexpositions
	thingalfredinterface --> controluseALFREDlearnartexpositions
	controluseALFREDlearnartexpositions --> controllearnartexpositions
	thingartexpositions --- controllearnartexpositions
	thingexpositionsinterface --> controllearnartexpositions

@enduml