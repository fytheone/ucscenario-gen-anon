
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Memory Health" as thingmemoryhealth
	entity "Game" as thinggame
	entity "Physical" as thingphysical
	actor "Olderperson" as actorOlderPerson
	boundary "Physical\nInterface" as thingphysicalinterface #grey
	boundary "Health\nInterface" as thinghealthinterface #grey
	control "Have Some\nGames How\nShow Physical" as controlhavegamephysical
	control "Have Some\nGames How\nShow Memory\nHealth" as controlhavegame

	thingmemoryhealth <.. thinggame
	thingphysical <.. thinggame
	thinggame <.. thinggame
	actorOlderPerson --- thingphysicalinterface
	actorOlderPerson --- thinghealthinterface
	thingphysical --- controlhavegamephysical
	thingphysicalinterface --> controlhavegamephysical
	thinggame --- controlhavegamephysical
	thinggame --- controlhavegame
	thingmemoryhealth --- controlhavegame
	thinghealthinterface --> controlhavegame

@enduml