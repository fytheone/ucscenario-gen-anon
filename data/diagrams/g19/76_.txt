
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Suggestion" as thingsuggestion
	entity "Neighbourhood" as thingneighbourhood
	entity "Sports Classes" as thingsportsclasses #grey
	actor "Olderperson" as actorOlderPerson
	boundary "Neighbourhood\nInterface" as thingneighbourhoodinterface #grey
	control "Get Suggestions\nFor Sports\nClasses In\nNeighbourhood" as controlgetsuggestion

	thingsportsclasses <.. thingsuggestion
	thingneighbourhood <.. thingsportsclasses
	actorOlderPerson --- thingneighbourhoodinterface
	thingsuggestion --- controlgetsuggestion
	thingsportsclasses --- controlgetsuggestion
	thingneighbourhood --- controlgetsuggestion
	thingneighbourhoodinterface --> controlgetsuggestion

@enduml