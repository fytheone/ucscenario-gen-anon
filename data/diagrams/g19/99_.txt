
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Suggestion" as thingsuggestion
	entity "Sport Classes" as thingsportclasses
	actor "Olderperson" as actorOlderPerson
	boundary "Classes\nInterface" as thingclassesinterface #grey
	control "Receive Suggestions\nAbout Local\nSport Classes" as controlreceivesuggestion

	thingsportclasses <.. thingsuggestion
	actorOlderPerson --- thingclassesinterface
	thingsuggestion --- controlreceivesuggestion
	thingsportclasses --- controlreceivesuggestion
	thingclassesinterface --> controlreceivesuggestion

@enduml