
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Contact" as thingcontact
	actor "Olderperson" as actorOlderPerson
	boundary "Face" as thingface
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	control "Alfred Enable" as controlenableALFREDhavefacefacecontact
	control "Have Alfred" as controlhaveALFREDenableALFREDhavefacefacecontact
	control "Face Contacts" as controlfacecontact
	control "Have Face" as controlhavefacefacecontact

	actorOlderPerson --- thingface
	actorOlderPerson --- thingalfredinterface
	controlhaveALFREDenableALFREDhavefacefacecontact --> controlenableALFREDhavefacefacecontact
	thingalfredinterface --> controlenableALFREDhavefacefacecontact
	thingALFRED --- controlenableALFREDhavefacefacecontact
	thingALFRED --- controlhaveALFREDenableALFREDhavefacefacecontact
	thingalfredinterface --> controlhaveALFREDenableALFREDhavefacefacecontact
	controlhavefacefacecontact --> controlfacecontact
	thingcontact --- controlfacecontact
	thingface --> controlfacecontact
	controlenableALFREDhavefacefacecontact --> controlhavefacefacecontact
	thingface --> controlhavefacefacecontact

@enduml