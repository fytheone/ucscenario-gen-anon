
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Spontaneous Question" as thingspontaneousquestion
	entity "Day" as thingday
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	circle "Certain Time" as thingcertaintime
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Day\nInterface" as thingdayinterface #grey
	control "Have Alfred" as controlhaveALFRED
	control "Spontaneous Questions\nProgramme During\nCertain Times\nOf The\nDay" as controlprogrammespontaneousquestioncertaintime

	thingcertaintime <.. thingspontaneousquestion
	thingday *-- thingcertaintime
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingdayinterface
	thingALFRED --- controlhaveALFRED
	thingalfredinterface --> controlhaveALFRED
	thingcertaintime --- controlprogrammespontaneousquestioncertaintime
	thingday --- controlprogrammespontaneousquestioncertaintime
	thingdayinterface --> controlprogrammespontaneousquestioncertaintime
	thingspontaneousquestion --- controlprogrammespontaneousquestioncertaintime

@enduml