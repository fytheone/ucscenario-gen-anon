
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Page" as thingpage
	actor "Fab User" as actorfabsuser
	boundary "Frontend Urls" as thingfrontendURLsfabsuser
	control "Accessing" as controlaccessreflectfrontendURLsfabsuserpage
	control "Reflect The\nPage" as controlreflectfrontendURLsfabsuserpage

	actorfabsuser --- thingfrontendURLsfabsuser
	thingfrontendURLsfabsuser --> controlaccessreflectfrontendURLsfabsuserpage
	controlaccessreflectfrontendURLsfabsuserpage --> controlreflectfrontendURLsfabsuserpage
	thingfrontendURLsfabsuser --> controlreflectfrontendURLsfabsuserpage
	thingpage --- controlreflectfrontendURLsfabsuserpage

@enduml