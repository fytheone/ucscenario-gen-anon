
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Language" as thinglanguage
	entity "Fabs Pages" as thingfabspages #grey
	actor "Fab User" as actorfabsuser

	thingfabspages <.. thinglanguage

@enduml