
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Office Codes" as thingofficecodes #grey
	entity "Office Names" as thingofficenames
	actor "Datum User" as actordatauser
	boundary "Codes\nInterface" as thingcodesinterface #grey
	control "See The\nOffice Names\nFrom Office\nCodes" as controlseeofficenames

	thingofficecodes <.. thingofficenames
	actordatauser --- thingcodesinterface
	thingofficenames --- controlseeofficenames
	thingofficecodes --- controlseeofficenames
	thingcodesinterface --> controlseeofficenames

@enduml