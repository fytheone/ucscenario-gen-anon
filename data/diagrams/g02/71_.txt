
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fabs Groups" as thingfabsgroups #grey
	entity "Frec Paradigm" as thingfrecparadigm #grey
	actor "Developer" as actordeveloper
	boundary "Paradigm\nInterface" as thingparadigminterface #grey
	control "Provide Fabs\nGroups Function\nUnder The\nFrec Paradigm" as controlprovidefabsgroups

	thingfrecparadigm <.. thingfabsgroups
	actordeveloper --- thingparadigminterface
	thingfabsgroups --- controlprovidefabsgroups
	thingfrecparadigm --- controlprovidefabsgroups
	thingparadigminterface --> controlprovidefabsgroups

@enduml