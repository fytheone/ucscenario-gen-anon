
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "C" as thingC
	entity "Dun" as thingDUNS
	entity "Record" as thingrecord
	entity "Actiontype" as thingActionTypes
	entity "D" as thingd
	entity "Sam" as thingSAM
	actor "actorwhose" as actorwhose #grey
	actor "User" as actoruser
	boundary "Duns Validation" as thingdunsvalidationswhosewhosewhoseuser
	boundary "Validation" as thingvalidationwhosewhosewhoseuser #grey
	control "Registered Accept\nRecords Actiontypes\nAre The\nDuns In\nSam" as controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM #grey
	control "Accept Records\nActiontypes Are\nC" as controlacceptvalidationwhosewhosewhoseuserrecordC
	control "Accept Records\nActiontypes Are\nD" as controlacceptvalidationwhosewhosewhoseuserrecordd
	control "Accept Records\nActiontypes Are\nB" as controlacceptdunsvalidationswhosewhosewhoseuserrecord

	thingActionTypes <.. thingrecord
	actoruser --- thingdunsvalidationswhosewhosewhoseuser
	actorwhose --- thingdunsvalidationswhosewhosewhoseuser
	actorwhose --- thingvalidationwhosewhosewhoseuser
	actoruser --- thingvalidationwhosewhosewhoseuser
	thingDUNS --- controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM
	thingSAM --- controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM
	thingvalidationwhosewhosewhoseuser --> controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM
	thingrecord --- controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM
	thingActionTypes --- controlacceptvalidationwhosewhosewhoseuserrecordDUNSSAM
	thingvalidationwhosewhosewhoseuser --> controlacceptvalidationwhosewhosewhoseuserrecordC
	thingC --- controlacceptvalidationwhosewhosewhoseuserrecordC
	thingrecord --- controlacceptvalidationwhosewhosewhoseuserrecordC
	thingActionTypes --- controlacceptvalidationwhosewhosewhoseuserrecordC
	thingd --- controlacceptvalidationwhosewhosewhoseuserrecordd
	thingvalidationwhosewhosewhoseuser --> controlacceptvalidationwhosewhosewhoseuserrecordd
	thingrecord --- controlacceptvalidationwhosewhosewhoseuserrecordd
	thingActionTypes --- controlacceptvalidationwhosewhosewhoseuserrecordd
	thingdunsvalidationswhosewhosewhoseuser --> controlacceptdunsvalidationswhosewhosewhoseuserrecord
	thingrecord --- controlacceptdunsvalidationswhosewhosewhoseuserrecord
	thingActionTypes --- controlacceptdunsvalidationswhosewhosewhoseuserrecord

@enduml