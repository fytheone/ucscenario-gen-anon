
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Similarity" as thingsimilarity
	entity "Group Subjects" as thinggroupsubjects #grey
	actor "Zooniverse Admin" as actorzooniverseadmin
	boundary "Similarity\nInterface" as thingsimilarityinterface #grey
	control "To Group\nSubjects By\nSimilarity" as controltogroupsubjects

	thingsimilarity <.. thinggroupsubjects
	actorzooniverseadmin --- thingsimilarityinterface
	thinggroupsubjects --- controltogroupsubjects
	thingsimilarity --- controltogroupsubjects
	thingsimilarityinterface --> controltogroupsubjects

@enduml