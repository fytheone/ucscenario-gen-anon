
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Brand" as thingbrand
	entity "Video" as thingvideo
	entity "Related Information" as thingrelatedinformation
	actor "User" as actoruser
	boundary "Video\nInterface" as thingvideointerface #grey
	boundary "Brand\nInterface" as thingbrandinterface #grey
	control "Identify Brands\nIn Videos" as controlidentifybrandvideo
	control "Receive Related\nInformation About\nBrands" as controlreceiverelatedinformation

	thingvideo <.. thingbrand
	thingbrand <.. thingrelatedinformation
	actoruser --- thingvideointerface
	actoruser --- thingbrandinterface
	thingbrand --- controlidentifybrandvideo
	thingvideo --- controlidentifybrandvideo
	thingvideointerface --> controlidentifybrandvideo
	thingrelatedinformation --- controlreceiverelatedinformation
	thingbrandinterface --> controlreceiverelatedinformation
	thingbrand --- controlreceiverelatedinformation

@enduml