
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Visualisation" as thingvisualisation
	entity "Textual Description" as thingtextualdescription
	actor "Data ,\nConsume User" as actordataconsuminguser
	boundary "Visualisation\nInterface" as thingvisualisationinterface #grey
	control "See Textual\nDescriptions That\nEmbedded Visualisations" as controlseetextualdescription

	thingvisualisation <.. thingtextualdescription
	actordataconsuminguser --- thingvisualisationinterface
	thingtextualdescription --- controlseetextualdescription
	thingvisualisation --- controlseetextualdescription
	thingvisualisationinterface --> controlseetextualdescription

@enduml