
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Query" as thingquery
	entity "Polygon" as thingpolygon
	actor "Api User" as actorapiuser
	boundary "Query\nInterface" as thingqueryinterface #grey
	control "Request Polygons\nOn The\nQuery" as controlrequestpolygon

	thingquery <.. thingpolygon
	actorapiuser --- thingqueryinterface
	thingpolygon --- controlrequestpolygon
	thingquery --- controlrequestpolygon
	thingqueryinterface --> controlrequestpolygon

@enduml