
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Viewer" as thingviewer
	entity "Embed Mode" as thingembedmode
	entity "Data Types\nHierarchies" as thingdatatypeshierarchies #grey
	actor "Platform Administrator" as actorplatformadministrator
	boundary "Mode\nInterface" as thingmodeinterface #grey
	control "Translate The\nData Types\nHierarchies Of\nThe Viewer\nIn Embed\nMode" as controltranslatedatatypeshierarchiesembedmode

	thingembedmode <.. thingviewer
	thingviewer <.. thingdatatypeshierarchies
	actorplatformadministrator --- thingmodeinterface
	thingdatatypeshierarchies --- controltranslatedatatypeshierarchiesembedmode
	thingviewer --- controltranslatedatatypeshierarchiesembedmode
	thingembedmode --- controltranslatedatatypeshierarchiesembedmode
	thingmodeinterface --> controltranslatedatatypeshierarchiesembedmode

@enduml