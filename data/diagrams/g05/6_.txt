
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	entity "Data Source" as thingdatasource #grey
	actor "Data ,\nPublishing User" as actordatapublishinguser
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Edit The\nData Source\nOf Data" as controleditdatasource
	control "Imported" as controlimporteditdatasource

	thingdatum <.. thingdatasource
	actordatapublishinguser --- thingdatuminterface
	thingdatasource --- controleditdatasource
	thingdatum --- controleditdatasource
	controlimporteditdatasource --> controleditdatasource
	thingdatuminterface --> controleditdatasource
	thingdatuminterface --> controlimporteditdatasource

@enduml