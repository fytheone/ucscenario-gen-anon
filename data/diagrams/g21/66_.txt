
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Link" as thinglink
	entity "Registration Form" as thingregistrationform #grey
	entity "Menu" as thingmenu
	actor "Anonymoususer" as actoranonymoususer
	boundary "Form\nInterface" as thingforminterface #grey
	control "Find A\nLink In\nThe Menu\nLinks To\nThe Registration\nForm" as controlfindlinkmenu

	thingmenu <.. thinglink
	thingregistrationform <.. thingmenu
	actoranonymoususer --- thingforminterface
	thinglink --- controlfindlinkmenu
	thingmenu --- controlfindlinkmenu
	thingregistrationform --- controlfindlinkmenu
	thingforminterface --> controlfindlinkmenu

@enduml