
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Payment Information" as thingpaymentinformation #grey
	entity "Individual Sponsorship" as thingindividualsponsorship
	entity "Card" as thingcard
	actor "Authenticateduser" as actorauthenticateduser
	boundary "Information\nInterface" as thinginformationinterface #grey
	control "Enter Payment\nInformation" as controlenterpaymentinformation
	control "Have" as controlhave

	actorauthenticateduser --- thinginformationinterface
	thingpaymentinformation --- controlenterpaymentinformation
	thinginformationinterface --> controlenterpaymentinformation

@enduml