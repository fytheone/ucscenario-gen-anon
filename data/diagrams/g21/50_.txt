
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Sponsorship Person" as thingsponsorshipperson #grey
	entity "Job" as thingjob
	entity "Sponsor" as thingsponsor
	actor "Anonymoususer" as actoranonymoususer
	boundary "Person\nInterface" as thingpersoninterface #grey
	boundary "Sponsor\nInterface" as thingsponsorinterface #grey
	control "Contact The\nSponsorship Person" as controlcontactsponsorshipperson
	control "Learn Posting\nJobs As\nA Sponsor" as controllearnjobsponsor
	control "Learn" as controllearncontactsponsorshipperson

	thingsponsor <.. thingjob
	actoranonymoususer --- thingpersoninterface
	actoranonymoususer --- thingsponsorinterface
	thingsponsorshipperson --- controlcontactsponsorshipperson
	controllearncontactsponsorshipperson --> controlcontactsponsorshipperson
	thingpersoninterface --> controlcontactsponsorshipperson
	thingjob --- controllearnjobsponsor
	thingsponsor --- controllearnjobsponsor
	thingsponsorinterface --> controllearnjobsponsor
	thingpersoninterface --> controllearncontactsponsorshipperson

@enduml