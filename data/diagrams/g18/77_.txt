
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Standard Metadata" as thingstandardmetadata
	entity "System" as thingsystem
	entity "Researcher" as thingbeyondresearcher
	entity "Centre" as thingcentre
	actor "Researcher" as actorresearcher
	boundary "System\nInterface" as thingsysteminterface #grey
	boundary "Metadata\nInterface" as thingmetadatainterface #grey
	control "Have The\nSystem" as controlhavesystemprovidestandardmetadata
	control "Provide Standard\nMetadata" as controlprovidestandardmetadata

	thingcentre <.. thingbeyondresearcher
	actorresearcher --- thingsysteminterface
	actorresearcher --- thingmetadatainterface
	thingsystem --- controlhavesystemprovidestandardmetadata
	thingsysteminterface --> controlhavesystemprovidestandardmetadata
	controlhavesystemprovidestandardmetadata --> controlprovidestandardmetadata
	thingstandardmetadata --- controlprovidestandardmetadata
	thingmetadatainterface --> controlprovidestandardmetadata

@enduml