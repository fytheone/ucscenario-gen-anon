
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Files" as thingdatafiles
	actor "User" as actoruser
	boundary "Files\nInterface" as thingfilesinterface #grey
	control "Make Uploaded\nData Files" as controlmakeuploadeddatafiles

	actoruser --- thingfilesinterface
	thingdatafiles --- controlmakeuploadeddatafiles
	thingfilesinterface --> controlmakeuploadeddatafiles

@enduml