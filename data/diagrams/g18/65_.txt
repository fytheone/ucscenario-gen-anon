
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Neurohub" as thingNeuroHub
	entity "Google Calendar" as thinggooglecalendar #grey
	entity "Event" as thingevent
	entity "Web ,\nBased Calendar" as thingwebbasedcalendar
	actor "User" as actoruser
	boundary "Calendar\nInterface" as thingcalendarinterface #grey
	control "Sync Events\nIn Neurohub\nWith A\nWeb, Based\nCalendar Such\nAs Google\nCalendar" as controlsyncevent

	thingwebbasedcalendar <.. thingNeuroHub
	thingNeuroHub <.. thingevent
	thingwebbasedcalendar <.. thingevent
	thinggooglecalendar <.. thingwebbasedcalendar
	actoruser --- thingcalendarinterface
	thingevent --- controlsyncevent
	thingNeuroHub --- controlsyncevent
	thingwebbasedcalendar --- controlsyncevent
	thinggooglecalendar --- controlsyncevent
	thingcalendarinterface --> controlsyncevent

@enduml