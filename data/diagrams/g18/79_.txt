
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Standard Datum" as thingstandarddatum #grey
	entity "Video" as thingvideo
	entity "Behavioural Observation" as thingbehaviouralobservation
	entity "Meta Data" as thingmetadata
	actor "Researcher" as actorresearcher
	boundary "Ability" as thingabilityattachstandardmetadataresearcher #grey
	control "Attach Standard\nData For\nVideo" as controlattachstandarddatumvideo
	control "Attach Standard\nMeta Data\nFor Behavioural\nObservations" as controlattachstandardmetadata
	control "Have" as controlhaveabilityattachstandardmetadataresearcher
	control "Attach Standard\nData For" as controlattachstandarddatum #grey

	thingbehaviouralobservation <.. thingmetadata
	actorresearcher --- thingabilityattachstandardmetadataresearcher
	thingstandarddatum --- controlattachstandarddatumvideo
	thingvideo --- controlattachstandarddatumvideo
	thingabilityattachstandardmetadataresearcher --> controlattachstandarddatumvideo
	controlhaveabilityattachstandardmetadataresearcher --> controlattachstandarddatumvideo
	thingabilityattachstandardmetadataresearcher --> controlattachstandardmetadata
	thingmetadata --- controlattachstandardmetadata
	thingbehaviouralobservation --- controlattachstandardmetadata
	controlhaveabilityattachstandardmetadataresearcher --> controlattachstandardmetadata
	thingabilityattachstandardmetadataresearcher --> controlhaveabilityattachstandardmetadataresearcher
	thingstandarddatum --- controlattachstandarddatum
	controlhaveabilityattachstandardmetadataresearcher --> controlattachstandarddatum

@enduml