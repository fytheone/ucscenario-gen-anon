
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Forward" as thingforward
	entity "Button" as thingbackbutton
	entity "Log Books" as thinglogbooks #grey
	entity "Browser" as thingbrowser
	entity "Book Pages" as thingbookpages #grey
	actor "User" as actoruser
	boundary "Forward\nInterface" as thingforwardinterface #grey
	boundary "Pages\nInterface" as thingpagesinterface #grey
	boundary "Books\nInterface" as thingbooksinterface #grey
	boundary "Button\nInterface" as thingbuttoninterface #grey
	control "Backwards Navigate\nForwards Between" as controlnavigateforwardlogbookpagesusebackbutton
	control "Log Book\nPages Having" as controllogbookpagesusebackbutton
	control "Backwards Navigate\nForwards Between\nLog Books" as controlnavigateforwardlogbooks
	control "Use Browser's\nBack Button" as controlusebackbutton

	thinglogbooks <.. thingforward
	thingbrowser <.. thingbackbutton
	actoruser --- thingforwardinterface
	actoruser --- thingpagesinterface
	actoruser --- thingbooksinterface
	actoruser --- thingbuttoninterface
	thingforwardinterface --> controlnavigateforwardlogbookpagesusebackbutton
	thingforward --- controlnavigateforwardlogbookpagesusebackbutton
	controlnavigateforwardlogbookpagesusebackbutton --> controllogbookpagesusebackbutton
	thingbookpages --- controllogbookpagesusebackbutton
	thingpagesinterface --> controllogbookpagesusebackbutton
	thingforward --- controlnavigateforwardlogbooks
	thinglogbooks --- controlnavigateforwardlogbooks
	thingbooksinterface --> controlnavigateforwardlogbooks
	controllogbookpagesusebackbutton --> controlusebackbutton
	thingbackbutton --- controlusebackbutton
	thingbuttoninterface --> controlusebackbutton

@enduml