
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Log Book\nPage" as thinglogbookpage #grey
	entity "Table" as thingtable
	entity "Html Tags" as thinghtmltags #grey
	actor "User" as actoruser
	circle "Content" as thingcontent
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Draw Tables\nUsing Html\nTags As\nPart Of\nThe Content\nOf A\nLog Book\nPage" as controldrawtablehtmltagspart

	thinghtmltags <.. thingtable
	thingcontent <.. thinghtmltags
	thinglogbookpage *-- thingcontent
	actoruser --- thingpageinterface
	thingtable --- controldrawtablehtmltagspart
	thinghtmltags --- controldrawtablehtmltagspart
	thingcontent --- controldrawtablehtmltagspart
	thinglogbookpage --- controldrawtablehtmltagspart
	thingpageinterface --> controldrawtablehtmltagspart

@enduml