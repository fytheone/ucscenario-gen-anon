
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Attach File" as thingattachedfile
	entity "Log Book\nPage" as thinglogbookpage #grey
	actor "Researcher" as actorresearcher
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Attach Currently\nAttached Files\nTo A\nLog Book\nPage" as controlattachattachedfilelogbookpage

	thinglogbookpage <.. thingattachedfile
	actorresearcher --- thingpageinterface
	thingattachedfile --- controlattachattachedfilelogbookpage
	thinglogbookpage --- controlattachattachedfilelogbookpage
	thingpageinterface --> controlattachattachedfilelogbookpage

@enduml