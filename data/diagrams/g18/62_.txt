
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Workflow Execution" as thingworkflowexecution
	entity "Neurohub Log\nBook" as thingneurohublogbook #grey
	entity "Workflow" as thingworkflow
	actor "User" as actoruser
	boundary "Detail" as thingdetailworkflowexecutionuser
	control "Have" as controlhaverecorddetailworkflowexecutionuserneurohublogbookworkflow
	control "Recorded In\nA Neurohub\nLog Book\nExecuting A\nWorkflow" as controlrecorddetailworkflowexecutionuserneurohublogbookworkflow

	actoruser --- thingdetailworkflowexecutionuser
	thingdetailworkflowexecutionuser --> controlhaverecorddetailworkflowexecutionuserneurohublogbookworkflow
	controlhaverecorddetailworkflowexecutionuserneurohublogbookworkflow --> controlrecorddetailworkflowexecutionuserneurohublogbookworkflow
	thingdetailworkflowexecutionuser --> controlrecorddetailworkflowexecutionuserneurohublogbookworkflow
	thingworkflowexecution --- controlrecorddetailworkflowexecutionuserneurohublogbookworkflow
	thingneurohublogbook --- controlrecorddetailworkflowexecutionuserneurohublogbookworkflow
	thingworkflow --- controlrecorddetailworkflowexecutionuserneurohublogbookworkflow

@enduml