
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Content Management\nSystem Decision" as thingcontentmanagementsystemdecision #grey
	entity "Feature" as thingfeature
	entity "Key Lesson" as thingkeylesson
	actor "Nsf Employee" as actornsfemployee
	boundary "Final Workshop" as thingfinalworkshopidentifyfeaturensfemployee #grey
	control "Have" as controlhavefinalworkshopidentifykeylessonnsfemployee
	control "Identify Key\nLessons For\nThe Content\nManagement System\nDecision" as controlidentifykeylesson
	control "Identify" as controlidentify #grey
	control "Identify Feature" as controlidentifyfeature

	thingcontentmanagementsystemdecision <.. thingkeylesson
	actornsfemployee --- thingfinalworkshopidentifyfeaturensfemployee
	thingfinalworkshopidentifyfeaturensfemployee --> controlhavefinalworkshopidentifykeylessonnsfemployee
	controlhavefinalworkshopidentifykeylessonnsfemployee --> controlidentifykeylesson
	thingkeylesson --- controlidentifykeylesson
	thingcontentmanagementsystemdecision --- controlidentifykeylesson
	thingfinalworkshopidentifyfeaturensfemployee --> controlidentifykeylesson
	controlhavefinalworkshopidentifykeylessonnsfemployee --> controlidentify
	thingfeature --- controlidentifyfeature
	thingfinalworkshopidentifyfeaturensfemployee --> controlidentifyfeature
	controlhavefinalworkshopidentifykeylessonnsfemployee --> controlidentifyfeature

@enduml