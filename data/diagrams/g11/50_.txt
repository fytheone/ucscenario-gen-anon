
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Different Style" as thingdifferentstyle
	entity "Nsf .\nGov Website" as thingnsf.govwebsite
	actor "Visualdesigner" as actorVisualDesigner
	boundary "Website\nInterface" as thingwebsiteinterface #grey
	control "Concept Different\nStyles For\nThe Nsf.Gov\nWebsite" as controlconceptdifferentstyle

	thingnsf.govwebsite <.. thingdifferentstyle
	actorVisualDesigner --- thingwebsiteinterface
	thingdifferentstyle --- controlconceptdifferentstyle
	thingnsf.govwebsite --- controlconceptdifferentstyle
	thingwebsiteinterface --> controlconceptdifferentstyle

@enduml