import spacy
import subprocess
import os
from importlib.util import find_spec
from datetime import datetime
from typing import List
import time
import traceback

# The Django app launches from a different location so this a workaround for that is included in most of the files.
use_src = find_spec("src")
if use_src:
    from src.storyparser.tree_parser import TreeParser
    from src.uml.diagram import Diagram
    from src.util import Logger
    from src.storyparser.entity import Entity, EntityType, Property
    from src.preprocessing.preprocessing import Preprocessing


else:
    import sys
    sys.path.insert(0, "../")
    from storyparser.tree_parser import TreeParser
    from uml.diagram import Diagram
    from util import Logger
    from storyparser.entity import Entity, EntityType, Property
    from preprocessing.preprocessing import Preprocessing




class DiagramGenerator:

    nlp = spacy.load("en_core_web_md")
    plant_uml = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "plant_uml", "plantuml.jar"))

    @staticmethod
    def parse(story: str, logger: Logger, parser: TreeParser=None):
        if not parser:
            parser = TreeParser()
        doc_spacy = DiagramGenerator.nlp(story)
        #dependancy_tree = doc_spacy.print_tree()
        parser.entities(doc_spacy, logger,DiagramGenerator.nlp)
        
        return parser

    @staticmethod
    def make(parser, logger, id, stories: List[str]=None, display_all: bool=False):
        try:
            
            print("ents : ")
            for e in parser.all_ents:
                e.print()
                if(e.entity_type == EntityType.composite):
                    print("COMPOSIT")
                    e.get_children_relation()
                print("-----")
            print([n.root for n in parser.all_ents])

            """
            print("Root and lem : ")
            for e in parser.all_ents:
                print(e._root ," , ", e._lemma  )
                print("-----")
            print([e._root for e in parser.all_ents])
            for ent_story, prim_act in parser.ents_by_story:
                print("ACTOR : ")

                for act in prim_act:
                    act.print()
                    print("---")
                print("OTHER")
                for act in ent_story:
                    act.print()
                    print("---")
            """
            diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)
            diagram_out = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out", id + ".txt"))
            diagram_file = open(diagram_out, "w+")
            diagram_file.write(diagram.textual_representation)
            diagram_file.close()
            time.sleep(0.2)
            print(" test : "),
            print(DiagramGenerator.plant_uml)
            print(diagram_out)
            print(subprocess.check_output(["bash", "-c", "java -jar '" + DiagramGenerator.plant_uml + "' '" + diagram_out + "'"]))
        except Exception as e:
            logger.log("error", traceback.format_exc())

    @staticmethod
    def print_log(logger, story_id):
        logger.print_to_file(
            os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out")),
            story_id + "_LOG.txt"
        )

    @staticmethod
    def generate(story: str):
        story_id = str(datetime.now().timestamp())
        print("STORY : ",story)
        parser = TreeParser()

        logger = Logger(story_id)
        story = story.replace("\n", "")
        story_preprocess = Preprocessing().process([story])
        story_split,_ = story_preprocess[0]
        for s in story_split:
            p = DiagramGenerator.parse(s, logger, parser=parser)
        DiagramGenerator.make(p, logger, story_id, stories=[story])
        DiagramGenerator.print_log(logger, story_id)
        return story_id  
    #TODO OLd version 
    """
    @staticmethod
    def generate(story: str):
        story_id = str(datetime.now().timestamp())
        print("STORY : ",story)
        logger = Logger(story_id)
        story = story.replace("\n", "")
        p = DiagramGenerator.parse(story, logger)
        DiagramGenerator.make(p, logger, story_id, stories=[story])
        DiagramGenerator.print_log(logger, story_id)
        return story_id
    """
    @staticmethod
    def multi(stories: List[str], display_all: bool):
        print("DIAGRAM_TYPE", display_all)
        print("MULTI_US")
        parser = TreeParser()
        story_id = str(datetime.now().timestamp())
        logger = Logger(story_id)
        stories_split = []
        for story in stories:
            story = story.replace("\n", "")
            
            for  s in story.split("."):
                if(len(s) > 1):
                    s = s + "."
                    stories_split.append(s)
        stories = stories_split

        for story in stories:
            story_preprocess = Preprocessing().process([story])
            story_split,_ = story_preprocess[0]
            for s in story_split:
                p = DiagramGenerator.parse(s, logger, parser=parser)
             #DiagramGenerator.parse(story, logger, parser=parser)
        DiagramGenerator.make(p, logger, story_id, stories=stories, display_all=display_all)

        #DiagramGenerator.make(parser, logger, story_id, stories=stories, display_all=display_all)
        DiagramGenerator.print_log(logger, story_id)
        return story_id
