from django.shortcuts import render
from django.template.loader import render_to_string

# Create your views here.

def homepage(request):
	return render(request, "index/homepage.html", None)
