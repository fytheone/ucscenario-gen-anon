#!/bin/bash

nohup python3.6 manage.py runserver 0:2000 > my.log 2>&1 &
echo $! > daemon_pid.txt