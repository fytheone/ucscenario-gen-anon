from flask import Flask,send_file, request,jsonify
from flask_cors import CORS
from utils.diagram_generator_api import DiagramGenerator
app = Flask(__name__)
CORS(app)




@app.route('/uploads/<path:filename>')
def download_file(filename):
    """
    Returns the PNG related to the given path.
    """
    path = './' + filename+'.png'
    print("Send : ",path)
    return send_file(path, mimetype='image/jpg') 

@app.route('/create_simple', methods=[ 'POST'])
def create_simple():
    """
    Generates the diagram for the us sent in the request and places it in /single/
    Returns {
        id : PNG name,
        plan_uml : Plan_UML code used to generate the diagram
    }
    
    """
    rep = DiagramGenerator.generate(request.json["us"])
    return jsonify(rep)
    

@app.route('/create_multi', methods=['POST'])
def create_multi():
    """
    Generates the diagram for the us sent in the request and places it in /multi/
    Returns {
        id : PNG name,
        plan_uml : Plan_UML code used to generate the diagram
    }
    
    """
    stories = request.json["us"].split("\n")
    stories = [s for s in stories if s != ""]

    rep = DiagramGenerator.multi(stories,True)
    return jsonify(rep)

@app.route('/create_multi_split', methods=['POST'])
def create_multi_split():
    """
    Generates sub-diagrams for the us list sent in the query and places it in /multi_split/
    Returns the image id and the PLAN_UML code
    Returns {
        id_main : request id,
        list_view : [
            {
                name : id of the PNG 
                id : id of the PNG 
                plan_uml : Plan_UML code used to generate the diagram
            }
        ]
        
    }
    
    """
    stories = request.json["us"].split("\n")
    stories = [s for s in stories if s != ""]

    rep = DiagramGenerator.multi_split(stories)
    return jsonify(rep)

@app.route('/create_view_actor', methods=['POST'])
def create_view_actor():
    """
    Generates diagrams by actor for the us list sent in the query and places it in /actor/      
    Returns {
        id_main : request id,
        list_view : [
            {
                name : id of the PNG 
                id : id of the PNG 
                plan_uml : Plan_UML code used to generate the diagram
            }
        ]
        
    }
    """
    print("create_view_actor")
    stories = request.json["us"].split("\n")
    stories = [s for s in stories if s != ""]

    rep = DiagramGenerator.multi_view_by_object(stories,type_dia="view_actor")
    return jsonify(rep)

@app.route('/create_view_boundary', methods=['POST'])
def create_view_boundary():
    """
    Generates boundary by actor for the us list sent in the query and places it in /actor/      
    Returns {
        id_main : request id,
        list_view : [
            {
                name : id of the PNG 
                id : id of the PNG 
                plan_uml : Plan_UML code used to generate the diagram
            }
        ]
        
    }
    """
    print("create_view_boundary")
    stories = request.json["us"].split("\n")
    stories = [s for s in stories if s != ""]

    rep = DiagramGenerator.multi_view_by_object(stories,type_dia="view_boundary")
    return jsonify(rep)

@app.route('/create_view_entity', methods=['POST'])
def create_view_entity():
    """
    Generates diagrams by entity for the us list sent in the query and places it in /actor/      
    Returns {
        id_main : request id,
        list_view : [
            {
                name : id of the PNG 
                id : id of the PNG 
                plan_uml : Plan_UML code used to generate the diagram
            }
        ]
        
    }
    """
    print("create_view_entity")
    stories = request.json["us"].split("\n")
    stories = [s for s in stories if s != ""]

    rep = DiagramGenerator.multi_view_by_object(stories,type_dia="view_entity")
    return jsonify(rep)
    