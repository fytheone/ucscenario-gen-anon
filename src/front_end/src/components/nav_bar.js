

import React from 'react'
import { Navbar,Nav} from 'react-bootstrap';


const MainNavBar = (props)=>  {
    const diagram=props.diagram;
    
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href=".">Navbar</Navbar.Brand>
                <Nav className="mr-auto">
                <Nav.Link href="single">Single</Nav.Link>
                <Nav.Link href="multi">Multi</Nav.Link>
                <Nav.Link href="multi_split">Multi Split</Nav.Link>
                <Nav.Link href="byview">Multi View</Nav.Link>
                </Nav>
            </Navbar>
         </div>
    )

}



export default MainNavBar;