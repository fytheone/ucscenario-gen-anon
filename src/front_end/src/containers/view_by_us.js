import React,{Component} from "react";
import Diagram from"../containers/main_diagram"
import { Form,Button,DropdownButton,Dropdown,Col,Row} from 'react-bootstrap';
import axios from 'axios'

class ViewByStory extends Component {
    constructor(props){
        super(props)
        this.state={currentDiagram :{},currentStories:"",listDiagram:[],currentMainId:"",currentTypeView:"Actor"}
      }

    
    render(){
        console.log("on render ",this.state.currentTypeView)
        return (
            <div>
                Multi
                <Form>
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Control placeholder="As ..., I want ... so that ..." as="textarea" rows="3" onChange={this.handleChange.bind(this)}/>
                    </Form.Group>
                    <Row>
                        <Col><Button variant="dark"  onClick={() => {this.switchDiagram()}}>
                                Submit
                            </Button>
                        </Col>
                        <Col>
                            <DropdownButton
                            title={this.state.currentTypeView}
                            variant={"Secondary".toLowerCase()}
                            id={`dropdown-variants-Secondary`}
                            key={"Secondary"}
                            onChange={(e)=>
                                console.log(e)
                                }
                            >
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Actor"})} eventKey="1">Actor</Dropdown.Item>
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Entity"})} eventKey="2">Entity</Dropdown.Item>
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Boundary"})} eventKey="3">Boundary</Dropdown.Item>
                        </DropdownButton>
                        </Col>
                       
                    </Row>
                    
                    
                </Form>
                {this.renderListDiagram()}
            </div>
        )
    }


    renderListDiagram(){
        return(
        <div>
            {this.state.listDiagram.map((d)=>{
                console.log(d)
                var diagram = {
                    title : d["name"],
                    main_id:this.state.currentMainId,
                    id:d["id"],
                    type:"view_"+this.state.currentTypeView,
                    us:this.state.currentStories
                }
                return <Diagram diagram={diagram} key={d["id"]}/>
            })}
        </div>)
        
    }

    switchDiagram(){ 
        var URL_CREATE_DIAGRAMS = 'http://127.0.0.1:5000/create_view_'+this.state.currentTypeView.toLowerCase()    
        axios.post(URL_CREATE_DIAGRAMS,{us:this.state.currentStories})
          .then( (response) =>{
            console.log(response);

            var diagram = {
                title : this.state.currentStories,
                main_id:response["data"]["main_id"],
                type:"multi",
                us:this.state.currentStories
              }
            this.setState({currentDiagram:diagram})
            this.setState({currentMainId:response["data"]["main_id"]})
            this.setState({listDiagram:response["data"]["list_view"]})


          })
          .catch(function (error) {
            console.log(error);
          });


    }

    handleChange(event) {
        console.log( event.target.value)
        this.setState({ currentStories: event.target.value })
    }


}



export default ViewByStory;