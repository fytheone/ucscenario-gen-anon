import React,{Component} from "react";
import Diagram from"../containers/main_diagram"
import { Form,Button} from 'react-bootstrap';
import axios from 'axios'


class SimpleUS extends Component {
    constructor(props){
        super(props)
        this.state={currentDiagram :{},currentStory:""}
      }

    
    render(){
        console.log("on render")
        return (
            <div>
                <Form>
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Control placeholder="As ..., I want ... so that ..." as="textarea" rows="3" onChange={this.handleChange.bind(this)}/>
                    </Form.Group>
                    <Button variant="dark"  onClick={() => {this.switchDiagram()}}>
                        Submit
                    </Button>
                </Form>
                <Diagram diagram={this.state.currentDiagram}/>
            </div>
        )
    }

    switchDiagram(){          
        axios.post('http://127.0.0.1:5000/create_simple',{us:this.state.currentStory})
          .then( (response) =>{
            console.log(response);
            var diagram = {
                title : this.state.currentStory,
                id:response["data"]["id"],
                type:"single",
                text:response["data"]["plan_uml"],
                us:this.state.currentStory
              }
            this.setState({currentDiagram:diagram})
          })
          .catch(function (error) {
            console.log(error);
          });


    }

    handleChange(event) {
        this.setState({ currentStory: event.target.value })
    }


}



export default SimpleUS;