import React from 'react'

const URL_IMG="http://localhost:5000/uploads/out/"

const Diagram = (props)=>  {
    const diagram=props.diagram;
    var text = "\n"
    if(diagram.text){
        text = diagram.text
    }
        
    if(props.diagram.main_id){
        var URL_diagram = URL_IMG+diagram.type+"/"+diagram.main_id+"/"+diagram.id
    }else{
        var URL_diagram = URL_IMG+diagram.type+"/"+diagram.id
    }
    console.log(URL_diagram)    
    return (
    <div className="media">
        <div className="media-left">
            <img className="media-object img-rounded" src ={`${URL_diagram}`}/>
        </div>

        <div className="media-body">
            <h5 className="title_list_item" >{diagram.title}</h5>
        </div>
        <div>
{text.split("\n").map((item)=>(<div>{item}<br/> </div>))}
        </div>
        
    </div>
    )


}



export default Diagram;