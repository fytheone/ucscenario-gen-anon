from enum import Enum, auto


class PreprocessingEnums(Enum):
    clean_multiple_space = auto()
    lower_us = auto()   
    add_control = auto()
    split_by_conj = auto()
    split_by_conj_with_compl = auto()
    remove_unknow_actor = auto()
    replace_slash_by_or = auto()
    replace_hyphen_by_underscore = auto()
    replace_plus_by_underscore = auto()
    replace_double_quote_by_simple_quote = auto()
    remove_comma_before_conj = auto()
    replace_coref = auto()