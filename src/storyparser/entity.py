import warnings

from copy import copy, deepcopy
from enum import Enum, auto
from typing import List, Tuple, Optional


class EntityType(Enum):
    """
    List of entity types. Roughly correspond 1 to 1 with Diagram types.
    * EntityType.object = ObjectType.entity
    * EntityType.actor = ObjectType.actor
    * EntityType.relation = ObjectType.control
    * EntityType.extension = Used to store words for phrase reconstruction.
    * EntityType.statement = ObjectType.usecase
    * EntityType.composite = For marking composite classes. Largely obsoleted by the Composite and ReadOnlyComposite
                             classes. Should eventually be removed.
    * EntityType.actor_proxy = Not used. Intended for use with a proper co-referencing model if ever added.
    * EntityType.object_proxy = Not used. Intended for use with a proper co-referencing model if ever added.
    * EntityType.primary_actor_proxy = To be replaced with references to the primary actors.
    """

    object = auto()
    actor = auto()
    relation = auto()
    statement = auto()
    extension = auto()
    interface = auto()
    composite = auto()
    actor_proxy = auto()
    primary_actor_proxy = auto()
    object_proxy = auto()


    @staticmethod
    def proxies():
        return [EntityType.composite, EntityType.actor_proxy,
                EntityType.object_proxy, EntityType.primary_actor_proxy]

    def is_proxy(self):
        return self in self.proxies()


class Property:

    def __init__(self, name: str):
        self.name: str = name.lower()
        self._super_property: "Property" = None
        self._sub_property: "Property" = None

    @property
    def super_property(self):
        return self._super_property

    @super_property.setter
    def super_property(self, sp: "Property"):
        self._super_property = sp
        if sp:
            sp._sub_property = self

    @property
    def sub_property(self):
        return self._sub_property

    def formatted_str(self, pretty: bool=True):
        if pretty:
            return (self._super_property.formatted_str() + " " if self._super_property else "") + self.name.title()
        else:
            return (self._super_property.formatted_str(pretty=False) if self._super_property else "") + self.name

    def append(self, new_prop: "Property"):
        prop = self
        while prop._super_property:
            prop = prop._super_property
        prop._super_property = new_prop
        new_prop._sub_property = prop

    def all_variants(self) -> List[Tuple["Property", "Property"]]:
        if not self._super_property:
            return [(copy(self), None)]
        prop = copy(self._super_property)
        prop._sub_property = None
        result = [(Property(self.name), prop)]
        while prop:
            cpy = copy(result[-1][0])
            cpy.append(Property(prop.name))
            remainder = copy(prop._super_property)
            if remainder:
                remainder._sub_property = None
            result.append((cpy, remainder))
            prop = prop._super_property
        return result

    def __str__(self):
        return self.name.upper() + "(" + str(self._super_property) + ")"

    def __cpy_up(self):
        cpy = Property(self.name)
        if self._sub_property:
            sub = self._sub_property.__cpy_up()
            sub.super_property = cpy
        return cpy

    def __cpy_dwn(self):
        cpy = Property(self.name)
        if self._super_property:
            cpy.super_property = self._super_property.__cpy_dwn()
        return cpy

    def __copy__(self):
        cpy = Property(self.name)
        if self._super_property:
            cpy.super_property = self._super_property.__cpy_dwn()
        if self._sub_property:
            cpy._sub_property = self._sub_property.__cpy_up()
        return cpy


class Entity:
    """
    For storing parsed text data. Note that RELATIONS refers to any entity with a syntactic relation to this entity
    while CHILDREN refers to any entities contained inside this entity for the purpose of grouping. Additionally
    an Entity can only have one parent relation. If one has multiple syntactic relations use the duplicate()
    method to replicate it and any relevant relations.
    """

    def __init__(self, root_word: str, lemma, index, entity_type: EntityType = EntityType.extension, explicit: bool = True):
        self._pretext: List[str] = []
        self._property: Property = None
        self._description: List[str] = []
        self._lemma: str = lemma
        self._root: str = root_word
        self._case: str = ""
        self._post: List[str] = []
        self._entity_type: EntityType = entity_type
        self._relations: List[Entity] = []
        self._parent: Entity = None
        self._children: List[Entity] = []
        self.referenced_explicitly: bool = entity_type != EntityType.extension and explicit
        self._last_description_index = 0
        self.index = index #Index in the US
        self.is_property_object = False #True if the entity are an object who compose another object. (Ex: "a dataset of my website" dataset.is_property_object == True)

   
    def print(self):
        print(self._root,", ref_exp : ",self.referenced_explicitly ,", type: ",self._entity_type)
        if (self._parent):
            print("Parent : ",self._parent._root)
        print("Children : ")
        children_str = ""
        for c in self._children:
            children_str = children_str +", " + c._root
        print(children_str)
        print("Relation : ")
        rel_str = ""
        for c in self._relations:
            try:
                rel_str = rel_str +", " + c._lemma
            except AttributeError:
                print("Pas de lem") 
        print(rel_str)
        print("index : ",self.index)
        print("pretex : ",self.pretext)
        print("description : ",self.description)
        print("case : ",self.case)
        print("")



    @property
    def lemma(self) -> str:
        return self._lemma

    @lemma.setter
    def lemma(self, lemma: str):
        if self._children:
            for c in self._children:
                old = c.lemma
                c.lemma = lemma
                self._lemma.replace(old, c.lemma)
        else:
            self._lemma = lemma.lower()

    @property
    def root(self) -> str:
        return self._root

    @root.setter
    def root(self, root: str):
        if self._children:
            for c in self._children:
                old = c.root
                c.root = root
                self._root.replace(old, c.root)
        else:
            self._root = root.lower()

    @property
    def case(self) -> str:
        return self._case

    @case.setter
    def case(self, case: str):
        if self._children:
            for c in self._children:
                c.case = case
        else:
            self._case = case

    @property
    def entity_type(self) -> EntityType:
        return self._entity_type

    @entity_type.setter
    def entity_type(self, entity_type: EntityType):
        if self._children:
            for c in self.children:
                c.entity_type = entity_type
        else:
            self._entity_type = entity_type

    @property
    def pretext(self):
        return copy(self._pretext)

    def add_pretext(self, pretext: str, merge: bool = False):
        for child in self._children:
            child.add_pretext(pretext, merge)
        if merge and self._pretext:
            self._pretext[-1] += pretext.lower()
        else:
            self._pretext.append(pretext.lower())

    def remove_pretext(self, index: int = -1) -> str:
        if self._children:
            if self._pretext:
                del self._pretext[index]
            return " ".join([child.remove_pretext(index=index) for child in self._children])
        if self._pretext:
            return self._pretext.pop(index)

    @property
    def description(self):
        return copy(self._description)

    def add_description(self, description: str, prepend: bool = True):
        self._last_description_index = 0 if prepend else -1
        for child in self._children:
            child.add_description(description, prepend=prepend)
        if prepend:
            self._description.insert(0, description.lower())
        else:
            self._description.append(description.lower())

    def remove_description(self) -> str:
        if self._children:
            if self._description:
                del self._description[self._last_description_index]
            return " ".join([str(child.remove_description()) for child in self._children])
        if self._description:
            return self._description.pop(self._last_description_index)

    @property
    def properties(self) -> List[Property]:
        if self.children:
            return [p for c in self._children for p in c.properties]
        else:
            return [self._property] if self._property else []

    def add_property(self, prop: Property):
        for child in self._children:
            child.add_property(prop)
        self._property = prop

    def remove_property(self) -> List[Property]:
        result = [self._property] if self._property else []
        if self._children:
            return [p for child in self._children for p in child.remove_property()]
        self._property = None
        return result

    def base_property(self) -> Property:
        base = self._property
        while base and base.super_property:
            base = base.super_property
        return base

    @property
    def post(self):
        warnings.warn(
            "post text has been replaced with EntityType.extension type Entities added as a relation. Post"
            "functionality is not guaranteed to work.", DeprecationWarning
        )
        return copy(self._post)

    def add_post(self, post: str):
        warnings.warn(
            "post text has been replaced with EntityType.extension type Entities added as a relation. Post"
            "functionality is not guaranteed to work.", DeprecationWarning
        )
        for child in self._children:
            child.add_post(post)
        self._post.append(post.lower())

    def remove_post(self, index: int = -1) -> str:
        warnings.warn(
            "post text has been replaced with EntityType.extension type Entities added as a relation. Post"
            "functionality is not guaranteed to work.", DeprecationWarning
        )
        if self._children:
            if self._post:
                del self._post[index]
            return " ".join([child.remove_post(index=index) for child in self._children])
        if self._post:
            return self._post.pop(index)

    @property
    def parent(self) -> Optional["Entity"]:
        return self._parent

    @parent.setter
    def parent(self, parent: "Entity"):
        if self._children:
            for c in self._children:
                c.parent = parent
        else:
            self._parent = parent

    @property
    def relations(self):
        return copy(self._relations)

    def add_relation(self, entity: "Entity") -> Tuple["Entity", List["Entity"]]:
        """
        Adds an entity as a relation. Because compound entities contain multiple entities which each need to be
        connected to a unique relation and each of those relations also need updated with future changes this function
        groups them into a proxy entity which can be modified to modify all the copies. Note that adding to a composite
        entity attaches the given Entity to its children but adding a composite entity just aattachesthe composite
        entity.
        :param entity: The entity to add as a relation.
        :return: The added entity or a proxy containing all the added entities and the copies in a list.
        """

        if not self._children:
            if entity._parent:
                entity._parent.remove_relation(entity)
            self._relations.append(entity)
            entity._parent = self
            return entity, []

        else:
            copies = []
            new = []
            entity_copy = entity
            for child in self._children:
                if not entity_copy:
                    entity_copy, new_ents = entity.duplicate(dup_up=False, convert_type=False)
                    new += new_ents
                child.add_relation(entity_copy)
                copies.append(entity_copy)
                entity_copy = None
            if not self.entity_type == EntityType.composite:
                if not entity_copy:
                    entity_copy, new_ents = entity.duplicate(dup_up=False, convert_type=False)
                    new += new_ents
                self._relations.append(entity_copy)
                entity_copy._parent = self
                copies.append(entity_copy)

            entity_proxy = Composite.build_from(copies)
            return entity_proxy, new

    def remove_relation(self, entity: "Entity"):
        for child in self._children:
            child.remove_relation(entity)
        # todo this check should not be necessary
        if entity in self._relations:
            self._relations.remove(entity)
            entity._parent = None

    @property
    def children(self):
        return copy(self._children)

    def add_child(self, child: "Entity"):
        self._children.append(child)

    def full_text(self, include_pretext: bool = True, include_post: bool = True,
                  use_lemma: bool = False, include_prop: bool=True):
        """
        Returns the text contained in this entity.
        :param include_pretext: Include the pre-text.
        :param include_post: Include the post-text.
        :param use_lemma: Use the lemma of the word instead of the word as it was written in the story.
        :param include_prop: Include the properties.
        :return: Textual representation.
        """
        return " ".join(([pt.title() for pt in self._pretext] if include_pretext else []) +
                        [", ".join([d.title() for d in self._description])] +
                        [self._property.formatted_str() if include_prop and self._property else ""] +
                        [(self.root if (not use_lemma) or (include_prop and self._property) else self.lemma).title() + (
                            self.case if include_pretext else "")] +
                        ([p.title() for p in self._post] if include_post else []))

    def __str__(self):
        return "[" + ("Explicit " if self.referenced_explicitly else "Implicit ") + str(self.entity_type) + "] {" + \
               ", ".join(self._pretext) + "} " + \
               "(" + ", ".join(self._description) + ") " + " <" + str(self._property) + "> " \
               + self.root + " |" + ",".join(self._post) + "| " + self.lemma

    def __class_based_copy(self):
        """
        Class specific copy function (so that the entire duplicate function doesn't have to be repeated)
        :return: copy of this class
        """
        return Entity(self.root, self.lemma, self.entity_type, explicit=self.referenced_explicitly)

    def duplicate(self, dup_up=True, dup_down=True, copy_up=False, convert_type=True, ignore=None,
                  include_children=True, include_properties=True) -> Tuple["Entity", List["Entity"]]:
        """
        Duplicates this entity. Check default options before using as they significantly change the behaviour of this
        function.
        :param dup_up: Include duplicates of ALL parents.
        :param dup_down: Include duplicates of ALL relations.
        :param copy_up: Add to the parent of the item being duplicated.
        :param convert_type: Converts the type of the entity. In general leave this as if you plan on using the
        duplicate as a different object, False if it's supposed to be an exact replicate.
        :param ignore: List of nodes to ignore. This prevents duplication of already duplicted nodes. I.E. if
        duplicating up and down then the parent will duplicate all of its relation nodes EXCEPT the node this was called
        from. Strongly recommended NOT to change this value.
        :param include_children: Include all children.
        :param include_properties: Include properties.
        :return: duplicate entity.
        """
        if not ignore:
            ignore = []
        ignore.append(self)

        # return a duplicate of this proxy with duplicates of all children
        if include_children and self._children:
            children_dup, new = [], []
            for child in self._children:
                cd, n = child.duplicate(dup_up=dup_up, dup_down=dup_down, copy_up=copy_up, convert_type=convert_type)
                children_dup.append(cd)
                new += n
            proxy = Composite.build_from(children_dup)
            return proxy, new

        duplicate = self.__class_based_copy()
        new = [duplicate]
        duplicate._pretext = copy(self._pretext)
        duplicate._description = copy(self._description)
        duplicate._post = copy(self._post)
        duplicate.index = copy(self.index)
        duplicate.entity_type = self.entity_type

        if include_properties:
            duplicate._properties = copy(self.properties)

        if convert_type:
            if self.entity_type in [EntityType.actor_proxy, EntityType.primary_actor_proxy]:
                duplicate.entity_type = EntityType.actor
            elif self.entity_type == EntityType.object_proxy:
                duplicate.entity_type = EntityType.object

        if dup_down:
            for r in [r for r in self._relations if r not in ignore]:
                r, sub_relations = r.duplicate(dup_up=True, dup_down=True, ignore=ignore, convert_type=convert_type)
                new.append(r)
                duplicate.add_relation(r)
                new += sub_relations
        

        if dup_up:
            if self._parent and self._parent not in ignore:
                parent, sub_parents = self._parent.duplicate(dup_up=True, dup_down=True, ignore=ignore, convert_type=convert_type)
                parent.add_relation(duplicate)
                new.append(parent)
                new += sub_parents
                if self._parent.entity_type in [EntityType.relation, EntityType.statement]:
                    self._parent.add_child(parent)

        elif copy_up:
            if self._parent:
                self._parent.add_relation(duplicate)

        # Remove excess duplicates created by the different duplications (we want 1 duplicate per entity). 
        new_clean = []
        for ent in new : 
            new_clean = [e for e in new_clean if not e.is_equal(ent)] + [ent]
        new = new_clean

        # We remove the entities equal to duplicate.
        # We need this step because the main duplicate is sometimes deleted by the previous step
        new = [e for e in new if not e.is_equal(duplicate)] + [duplicate]

        return duplicate, new

    

    def __copy__(self):
        raise Exception("Attempting to copy an entity. Use Entity.duplicate() instead to capture any deep copies")

    def is_actor(self):
        self_is = self.entity_type in [EntityType.actor, EntityType.actor_proxy, EntityType.primary_actor_proxy]
        if self_is:
            return self_is
        elif self._children:
            return all(c.is_actor() for c in self._children)
        else:
            return False

    def same_as(self, entity: "Entity"):
        if self._children:
            return self._children[-1].same_as(entity)
        return self.lemma == entity.entity_type and all(d in entity._description for d in self._description) and \
               all(d in self._description for d in entity._description)

    def as_property(self) -> Property:
        p = Property(" ".join(self.description) + " " + self.root)
        p.super_properties = self.properties
        return p

    def is_equal(self,ent):
        equal = True
        equal = equal and self._pretext == ent._pretext
        equal = equal and self._property == ent._property
        equal = equal and self._pretext == ent._pretext
        equal = equal and self._description == ent._description
        equal = equal and self._lemma == ent._lemma
        equal = equal and self._root == ent._root
        equal = equal and self._case == ent._case
        equal = equal and self._post == ent._post
        equal = equal and self._entity_type == ent._entity_type
        equal = equal and self._parent == ent._parent
        equal = equal and self._children == ent._children
        equal = equal and self.referenced_explicitly == ent.referenced_explicitly
        equal = equal and self.index == ent.index
        equal = equal and self.is_property_object == ent.is_property_object
        return equal



        
class Composite(Entity):
    """
    Composite version of Entity. Functionality is largely identical for legacy reasons and the (I believe unused)
    possibility of Entities containing sub-entities before the Composite functionality was fully fleshed out.
    """

    @staticmethod
    def build_from(ents: List[Entity]) -> Entity:
        
        index = min([e.index for e in ents])
        ent = Composite(
            "(" + " and ".join([e.root for e in ents]) + ")",
            "(" + " and ".join([e.root for e in ents]) + ")",
            entity_type=EntityType.composite,
            index=index
        )
        for e in ents:
            ent.add_child(e)
        return ent

    def __class_based_copy(self):
        return Composite(self.root, self.lemma, self.entity_type, explicit=self.referenced_explicitly)

    

    
class RelationOnlyComposite(Composite):
    """
    Variation of Entity that only applies changes to the last child added unless that change is adding/removing
    relations. Useful for when you have a conjunction  e.g. Red truck and blue car. and want to be able to refer to
    them as a single unit, if for example adding them to another entity, but only want to apply "blue" to the car word.
    """

    @property
    def lemma(self) -> str:
        return self._lemma

    @lemma.setter
    def lemma(self, lemma: str):
        if self._children:
            c = self._children[-1]
            old = c.lemma
            c.lemma = lemma
            self._lemma.replace(old, c.lemma)
        else:
            self._lemma = lemma.lower()

    @property
    def root(self) -> str:
        return self._root

    @root.setter
    def root(self, root: str):
        if self._children:
            c = self._children[-1]
            old = c.root
            c.root = root
            self._root.replace(old, c.root)
        else:
            self._root = root.lower()

    @property
    def case(self) -> str:
        return self._case

    @case.setter
    def case(self, case: str):
        if self._children:
            self._children[-1].case = case
        else:
            self._case = case

    @property
    def entity_type(self) -> EntityType:
        return self._entity_type

    @entity_type.setter
    def entity_type(self, entity_type: EntityType):
        if self._children:
            self._children[-1].entity_type = entity_type
        else:
            self._entity_type = entity_type

    @property
    def pretext(self):
        return copy(self._pretext)

    def add_pretext(self, pretext: str, merge: bool = False):
        if self._children:
            self._children[-1].add_pretext(pretext, merge=merge)
        if merge and self._pretext:
            self._pretext[-1] += pretext.lower()
        else:
            self._pretext.append(pretext.lower())

    def remove_pretext(self, index: int = -1) -> str:
        if self._children:
            if self._pretext:
                del self._pretext[index]
            return self._children[-1].remove_pretext(index=index)
        if self._pretext:
            return self._pretext.pop(index)

    @property
    def description(self):
        return copy(self._description)

    def add_description(self, description: str, prepend: bool = True):
        self._last_description_index = 0 if prepend else -1
        if self._children:
            self._children[-1].add_description(description, prepend=prepend)
        if prepend:
            self._description.insert(0, description.lower())
        else:
            self._description.append(description.lower())

    def remove_description(self) -> str:
        if self._children:
            if self._description:
                del self._description[self._last_description_index]
            return self._children[-1].remove_description()
        if self._description:
            return self._description.pop(self._last_description_index)

    @property
    def post(self):
        return copy(self._post)

    def add_post(self, post: str):
        if self._children:
            self._children[-1].add_post(post)
        self._post.append(post.lower())

    def remove_post(self, index: int = -1) -> str:
        if self._children:
            if self._post:
                del self._post[index]
            self._children[-1].remove_post(index)
        if self._post:
            return self._post.pop(index)

    @property
    def properties(self) -> List[Property]:
        if self.children:
            return self._children[-1].properties
        else:
            return [self._property]

    def add_property(self, prop: Property):
        if self._children:
            self._children[-1].add_property(prop)
        self._property = prop

    def remove_property(self) -> List[Property]:
        result = [self._property] if self._property else []
        if self._children:
            return self._children[-1].remove_property()
        self._property = None
        return result

    @staticmethod
    def build_from(ents: List[Entity]) -> Entity:
        index = min([e.index for e in ents])
        
        ent = RelationOnlyComposite(
            "(" + " and ".join([e.root for e in ents]) + ")",
            "(" + " and ".join([e.root for e in ents]) + ")",
            entity_type=EntityType.composite,
            index = index
        )
        for e in ents:
            ent.add_child(e)
        return ent

    def __class_based_copy(self):
        return RelationOnlyComposite(self.root, self.lemma, self.entity_type, explicit=self.referenced_explicitly)

    def __str__(self):
        return "RO " + Entity.__str__(self)
