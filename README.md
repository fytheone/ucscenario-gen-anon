# Welcome to the repo of the "Automatic Generation of robustness diagrams from User Stories"

This (set of) tools is in beta version and still under improvement. The doc is rather minimal, but feel free to try it. 

## How to run it easy way:
### Install every thing
bash ./bash_file/install.sh

### Run Api
bash ./bash_file/run_api.sh

### Run FrontEnd React
bash ./bash_file/run_front_end.sh


## How to run it Full way:
###Install python requirement :
```
 pip install requirement.txt
```

###Download SpaCy model : 
```
 python -m spacy download en_core_web_md 
 python -m spacy download en
```

###Commands to execute to deploy the flask model generation API:
```
 cd ./src/api/
 export FLASK_APP=api.py
 flask run
 * Running on http://127.0.0.1:5000/
```
###Commands to execute to deploy the React frontend:
```
 cd ./src/front_end/
 npm install
 npm start
```

###Legacy Django :
```
 cd src/djangoserver/
 python3 manage.py runserver 0:2000
```

## Few housekeeping before running (issues to be fixed)

### Create missing folders
Before starting, you will need to create the following folders under the ``` src/api/out ``` 

- ```multi```

- ```multi_split```

- ```single```

- ```view_actor```

- ```view_boundary```

- ```view_entity```

## Known issues

* Despite the generation of views work, the front end is not able to load them. Simply browse under ``` src/api/out/view_XXX ``` folder to see them.
